#include "threadPool.hpp"
#include "Task.hpp"
#include "Log.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

int main()
{
    srand((unsigned long)time(nullptr) ^ getpid());
    // std::cout << "hello thread pool" << std::endl;
    // ThreadPool<Task> *tp = new ThreadPool<Task>();
    ThreadPool<Task>::getThreadPool()->run();

    while (true)
    {
        int x = rand() % 100 + 1;
        usleep(1231);
        int y = rand() % 25 + 1;
        Task t(x, y, [](int x, int y) -> int
               { return x + y; 
        });

        logMessage(DEBUG, "制作任务完成: %d + %d = ?", x, y);
        // std::cout << "制作任务完成: " << x << " + " << y << " = ? " << std::endl;
        // 推送任务到线程池
        ThreadPool<Task>::getThreadPool()->pushTask(t);
        sleep(1);
    }
    return 0;
}
