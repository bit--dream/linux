#include <iostream>
#include <thread>
#include <unistd.h>
#include <cstring>
#include <pthread.h>

using namespace std;

void fun()
{
    while(true)
    {
        cout << "hello new thread" << endl;
        sleep(1);
    }
}

int main()
{
    thread t(fun);
    thread t1(fun);
    thread t2(fun);
    thread t3(fun);

    while (true)
    {
        cout << "hello main thread" << endl;
        sleep(1);
    }
    
    return 0;
}

// __thread  修饰全局变量,结果是让每个线程各自拥有一个全局变量 -- 线程的局部存储
// __thread int g_val = 0;

// void *threadRoutine(void *args)
// { 
//     //pthread_detach(pthread_self());
//     //execl("/usr/bin/ls", "ls", nullptr);

//     while (true)
//     {
//         cout << (char *)args << " : " << g_val << " &: " << &g_val << endl;
//         g_val++;
//         sleep(1);
//         break;
//     }

//     pthread_exit((void*)11);

//     // int i = 3;
//     // while (i--)
//     // {
//     //     cout << "新线程 : " << (char *)args << " runing... " << pthread_self() << endl;
//     //     sleep(1);
//     // }
//     //return (void *)10;
// }

// int main()
// {

//     pthread_t tid;
//     pthread_create(&tid, nullptr, threadRoutine, (void *)"thread 1");

//     while(true)
//     {
//         cout << "main thread" << " : " << g_val << " &: " << &g_val << endl;
//         sleep(1);
//         break;
//     }
    
//     int n = pthread_join(tid, nullptr);
//     cout << "n : " << n << " errstring : " << strerror(n) << endl;
    // printf("%lu, %p\n", tid, tid);
    // pthread_cancel(tid);
    //cout << "main pid : " << pthread_self() << endl;
    //void *ret = nullptr;
    //pthread_join(tid, &ret); // 默认阻塞等待新线程退出

    // 新线程被取消,join的时候退出码是-1 #define PTHREAD_CANCELED ((void *) -1)
    // PTHREAD_CANCELED;
    //cout << "main thread wait done...  main qiut..., new thread quit... : " << (long long)ret << endl;

    // while (true)
    // {
    //     cout << "main线程 : " << " runing... " << endl;
    //     sleep(1);
    // }

//     return 0;
// }
