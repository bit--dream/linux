#include "pollServer.hpp"
#include <memory>

int main()
{
    std::unique_ptr<PollServer> pvr(new PollServer());
    pvr->Start();
    return 0;
}



// #include <unistd.h>
// #include <stdio.h>
// int main()
// {
//     struct pollfd poll_fd;
//     poll_fd.fd = 0;
//     poll_fd.events = POLLIN;
//     for(;;)
//     {
//         int ret = poll(&poll_fd, 1, 1000);
//         if(ret < 0)
//         {
//             perror("poll");
//             continue;
//         }
//         if(ret == 0)
//         {
//             printf("poll timeout\n");
//             continue;
//         }
//         if (poll_fd.revents == POLLIN)
//         {
//             printf("poll event ready ");
//             char buffer[1024] = {0};
//             read(0, buffer, sizeof(buffer) - 1);
//             printf("stdin:%s", buffer);
//         }

//     }
//     return 0;
// }











