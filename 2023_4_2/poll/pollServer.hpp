#ifndef _SELECT_SVR_H_
#define _SELECT_SVR_H_

#include <iostream>
#include <string>
#include <poll.h>
#include <sys/time.h>
#include "Log.hpp"
#include "Sock.hpp"

#define FD_NONE -1

using namespace std;

class PollServer
{
public:
    static const nfds_t nfds = 100;

public:
    PollServer(const uint16_t &port = 8080) : _port(port), _nfds(nfds)
    {
        _listenSock = Sock::Socket();
        Sock::Bind(_listenSock, _port);
        Sock::Listen(_listenSock);
        logMessage(DEBUG, "create base socket success");
        _fds = new pollfd[_nfds];

        for (int i = 0; i < _nfds; i++)
        {
            _fds[i].fd = FD_NONE;
            _fds[i].events = _fds[i].revents = 0;
        }
        _fds[0].fd = _listenSock;
        _fds[0].events = POLLIN;
        _timeout = 1000;
    }

    void Start() 
    {
        while (true)
        {
            int n = poll(_fds, _nfds, _timeout);
            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out ...");
                break;
            case -1:
                logMessage(WARNING, "poll error %d : %S", errno, strerror(errno));
                break;
            default:
                HandlerEvent();
                break;
            }
        }
    }
    ~PollServer()
    {
        if (_listenSock >= 0)
        {
            close(_listenSock);
        }
        if (_fds)
            delete[] _fds;
    }

private:
    void Accepter()
    {
        string clientip;
        uint16_t clientport = 0;
        int sock = Sock::Accept(_listenSock, &clientport, &clientip);
        if(sock < 0)
        {
            logMessage(WARNING, "accept error");
        }
        logMessage(DEBUG, "get a new line success : [%s:%d] : %d", clientip.c_str(), clientport, sock);
        int pos = 1;
        for(;pos <_nfds; pos++)
        {
            if(_fds[pos].fd == FD_NONE) break;
        }
        if(pos == _nfds) 
        {
            // 可以对struct pollfd进行自动扩容
            logMessage(WARNING, "poll server already full, close: %d", sock);
            close(sock);
        }
        else
        {
            _fds[pos].fd = sock;
            _fds[pos].events = POLLIN;
        }
    }

    void Recver(int i)
    {
        logMessage(DEBUG, "message in, get IO event : %d", _fds[i]);
        char buffer[1024];
        int n = recv(_fds[i].fd, buffer, sizeof(buffer) - 1, 0);
        if(n > 0)
        {
            buffer[n] = 0;
            logMessage(DEBUG, "client[%d] # %s", _fds[i], buffer);
        }
        else if(n == 0)
        {
            logMessage(DEBUG, "client[%d] quit, me too ...", _fds[i].fd);
            close(_fds[i].fd);
            _fds[i].events = 0;
        }
        else
        {
            logMessage(WARNING, "%d sock recv errror, %d : %s", _fds[i].fd, errno, strerror(errno));
            close(_fds[i].fd);
            _fds[i].events = 0;
        }
    }
    void HandlerEvent()
    {
        for (int i = 0; i < _nfds; i++)
        {
            // 1.去掉不合法的fd
            if (_fds[i].fd == FD_NONE)
                continue;
            // 2.判断是否就绪
            if (_fds[i].revents & POLLIN)
            {
                if (_fds[i].fd == _listenSock)
                    Accepter();
                else
                    Recver(i);
            }
        }
    }
    void DebugPrint()
    {
        cout << "_fds[] :";
        for(int i = 0; i < _nfds; i ++)
        {
            if(_fds[i].fd == FD_NONE) continue;
            cout << _fds[i].fd << " ";
        }
        cout << endl;
    }

private:
    uint16_t _port;
    int _listenSock;
    struct pollfd *_fds;
    nfds_t _nfds;
    int _timeout;
};

#endif