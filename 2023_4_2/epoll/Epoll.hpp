#pragma once

#include <iostream>
#include <sys/epoll.h>
#include <unistd.h>

class Epoll
{
public:
    static const int g_size = 256;

public:
    static int CreateEpoll()
    {
        int epfd = epoll_create(g_size);
        if(epfd > 0) return epfd;
        exit(5); 
    }
    static bool CtlEpoll(int epfd, int oper, int sock, uint32_t events)
    {
        struct epoll_event ev;
        ev.events = events;
        ev.data.fd = sock;

        int e = epoll_ctl(epfd, oper, sock, &ev);
        return e == 0;
    }
    // 1. 如果底层就绪的sock非常多,revs装不下怎么办?? 不影响,一次拿不完,下次再拿
    // 2. 关于epoll_wait的返回值:有几个fd上的事件就绪,就返回几,epoll返回的时候,会将所有
    //    就绪的event按照顺序放入revs数组中,一个有返回值个
    static int WaitEpoll(int epfd, struct epoll_event revs[], int num, int timeout)
    {
        return epoll_wait(epfd, revs, num, timeout);
    }
private:
};
