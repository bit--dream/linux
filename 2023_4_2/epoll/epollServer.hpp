#ifndef _EPOLL_SERVER_HPP_
#define _EPOLL_SERVER_HPP_

#include <iostream>
#include <string>
#include <functional>
#include <cassert>
#include "Log.hpp"
#include "Sock.hpp"
#include "Epoll.hpp"

using namespace std;

class EpollServer
{
public:
    static const int default_port = 8080;
    static const int g_num = 64;
    using func_t = function<void(string)>;
public:
    EpollServer(func_t HandlerRequest, const uint16_t &port = default_port)
        : _port(port),
          _revsNum(g_num),
          _HandlerRequest(HandlerRequest)
    {
        // 0. 申请对应的空间
        _revs = new struct epoll_event[_revsNum];
        // 1. 创建listensock
        _listenSock = Sock::Socket();
        Sock::Bind(_listenSock, _port);
        Sock::Listen(_listenSock);

        // 2. 创建epoll模型
        _epfd = Epoll::CreateEpoll();
        logMessage(DEBUG, "init success, listensock :  %d, epfd : %d", _listenSock, _epfd);

        // 3. 将listensock,先添加到epool中, 让epool帮我们管理
        if (!Epoll::CtlEpoll(_epfd, EPOLL_CTL_ADD, _listenSock, EPOLLIN))
            exit(6);

        logMessage(DEBUG, "add listensock to epoll success");
    }

    ~EpollServer()
    {
        if (_listenSock >= 0)
            close(_listenSock);
        if (_epfd >= 0)
            close(_epfd);
        if (_revs)
        {
            delete[] _revs;
            _revs = nullptr;
        }
    }
    void Accepter(int listensock)
    {
        uint16_t clientport;
        string clientip;
        int sock = Sock::Accept(listensock, &clientport, &clientip);
        if(sock  < 0)
        {
            logMessage(WARNING, "accept error");
            return;
        }
        // 拿上链接,不能直接读取, 因为不清楚底层是否有数据
        // 将新的sock,添加到epoll
        if (!Epoll::CtlEpoll(_epfd, EPOLL_CTL_ADD, sock, EPOLLIN))
            return;
        logMessage(DEBUG,"add new sock : %d to epoll success", sock);

    }

    void Recver(int sock)
    {
        // 1. 读取数据
        char buffer[10240];
        ssize_t n = recv(sock, buffer,sizeof(buffer) - 1, 0);
        if(n > 0)
        {
            //假设这里读到的就是一个完整的报文
            buffer[n] = 0;
            // 2. 处理数据
            _HandlerRequest(buffer);
        }
        else if(n == 0)
        {
            // 先在epoll中移除对sock的关心
            bool ans = Epoll::CtlEpoll(_epfd, EPOLL_CTL_DEL, sock, 0);
            assert(ans);
            (void)ans;
            // 再关闭sock
            close(sock);
            logMessage(NORMAL, "client %d quit,me too ...", sock);
        }
        else
        {
            // 先在epoll中移除对sock的关心
            bool ans = Epoll::CtlEpoll(_epfd, EPOLL_CTL_DEL, sock, 0);
            assert(ans);
            (void)ans;
            // 再关闭sock
            close(sock);
            logMessage(NORMAL, "recv %d error, close error sock ", sock);
        }
        
    }
    void HandlerEvents(int n)
    {
        assert(n > 0);
        for(int i = 0; i < n; i ++)
        {
            uint32_t revents = _revs[i].events;
            int sock = _revs[i].data.fd;
            // 读事件就绪
            if(revents & EPOLLIN)
            {
                // 1.listensock 就绪
                if(sock == _listenSock) Accepter(_listenSock);
                // 2.一般sock就绪
                else Recver(sock);
            }
        }
    }

    void LoopOnce(int timeout)
    {
        int n = Epoll::WaitEpoll(_epfd, _revs, _revsNum, timeout);

        // if(n == _revsNum) 扩容

        switch (n)
        {
        case 0:
            logMessage(DEBUG, "time out ...");
            break;
        case -1:
            logMessage(WARNING, "epoll wait error :[%d : %s]", errno, strerror(errno));
            break;
        default:
            // 等待成功
            logMessage(DEBUG, "get a event");
            HandlerEvents(n);
            break;
        }
    }
    void Start()
    {
        int timeout = 1000;
        while (true)
        {
            LoopOnce(timeout);
        }
    }

private:
    int _listenSock;
    int _epfd;
    uint16_t _port;
    struct epoll_event *_revs;
    int _revsNum;
    func_t _HandlerRequest;
};

#endif
