#pragma once

#include <iostream>
#include <functional>
#include <string>
#include <unordered_map>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"

class TcpServer;

// 为了能够正常工作,常规的sock必须要有自己独立的接收缓冲区&&发送缓冲区
class Connection
{
    using func_t = std::function<void(Connection *)>;

public:
    Connection(int sock = -1)
        : _sock(sock), _tsvr(nullptr)
    {
    }

    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    int _sock; // 负责进行IO的文件描述符

    func_t _recv_cb;   // 读回调
    func_t _send_cb;   // 写回调
    func_t _except_cb; // 异常回调

    std::string _inbuffer; // 暂时没有办法处理二进制流,文本是可以的
    std::string _outbuffer;

    TcpServer *_tsvr; // 对TcpServer的回值指针
};

void Accepter()
{

}

class TcpServer
{
    const static uint16_t gport = 8080;

public:
    TcpServer(uint16_t port = gport)
        : _port(port)
    {
        // 1. 创建listensock
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 2. 创建epoll对象
        _poll.CreateEpoll();

        // 3. 
        Connection *conn = new Connection(_listensock);
        conn->SetCallBack(nullptr, nullptr, nullptr);
        conn->_tsvr = this;

        // 4. a.添加sock到epoll模型  b.将对应的Connection*对象指针添加到Connections集合中

    }
    ~TcpServer()
    {
        if(_listensock >= 0) close(_listensock);
    }

private:
    int _listensock;
    uint16_t _port;
    Epoll _poll;
    // sock : connection
    std::unordered_map<int, Connection*> _connections;
};
