#pragma once
#include <iostream>
#include <sys/epoll.h>

class Epoll
{
    const static int gnum = 128;

public:
    Epoll()
    {
    }
    void CreateEpoll()
    {
        _epfd = epoll_create(gnum);
        if (_epfd < 0)
            exit(5);
    }
    ~Epoll()
    {
    }

private:
    int _epfd;
};