#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main()
{
    printf("stdin:%d\n",stdin->_fileno);
    printf("stdout:%d\n",stdout->_fileno);
    printf("stderr:%d\n",stderr->_fileno);


    //char input[16];
    //
    //ssize_t ret = read(0, input, sizeof(input));
    //if(ret > 0)
    //{
    //    input[ret] = '\0';
    //    printf("%s\n", input);
    //}

    //int a = 10;
    //fscanf(stdin, "%d", &a);
    //printf("%d\n", a);


   // fprintf(stdout, "hello stdout\n");

   // const char* s = "hello 1\n";
   // write(1, s, strlen(s));
   
    // printf("stdin: %d\n", stdin->_fileno);
   // printf("stdout: %d\n", stdout->_fileno);
   // printf("stderr: %d\n", stderr->_fileno);
   // umask(0);
   // int fd = open("log.txt", O_WRONLY|O_CREAT, 0666);// rw-rw-rw-
   // if(fd < 0)
   // {
   //     perror("open");
   //     return 1;
   // }

   // printf("open success, fd: %d\n", fd);
   // return 0;
}



// 用int中的不重复的一个bit，就可以标识一种状态
//#define ONE 0x1   //0000 0001
//#define TWO 0x2   //0000 0010
//#define THREE 0x4 //0000 0100
//
//void show(int flags) //0000 0101
//{
//    if(flags & ONE)  printf("hello one\n"); // 0000 0011 & 0000 0001
//    if(flags & TWO)   printf("hello two\n");
//    if(flags & THREE) printf("hello three\n");
//}
//
//
//int main()
//{
//    show(ONE);
//    printf("-----------------------------------------\n");
//    show(TWO);
//    printf("-----------------------------------------\n");
//    show(ONE | TWO); //000 0001 | 0000 0010 = 0000 0011
//    printf("-----------------------------------------\n");
//    show(ONE | TWO | THREE);
//    printf("-----------------------------------------\n");
//    show(ONE | THREE);
//    printf("-----------------------------------------\n");
//    return 0;
//}

//int main(int argc, char* argv[])
//{
//    if(argc != 2)
//    {
//        printf("argv error\n");
//        return 1;
//    }
//
//    FILE* fp = fopen(argv[1], "r");
//    //FILE* fp = fopen("log.txt", "r");
//    if(fp == NULL)
//    {
//        perror("fopen");
//        return 2;
//    }
//
//
//    //按行读取
//    char line[20];
//
//    while(fgets(line, sizeof(line), fp) != NULL)
//    {
//        fprintf(stdout, "%s", line);
//    }
//
//    //文件操作
//    //const char *s1 = "hello 105\n"; //不要+1, 文件要保存的是有效数据
//    //fwrite(s1, strlen(s1), 1, fp);
//
//    //const char *s2 = "hello fprintf\n";
//    //fprintf(fp, "%s", s2);
//   
//    //const char *s3 = "hello fputs\n";
//    //fputs(s3, fp);
//    //fclose(fp);
//
//    //while(1)
//    //{
//    //    sleep(1);
//    //}
//    return 0;
//}
//
