#pragma once
#include <iostream>
#include <sys/epoll.h>

class Epoll
{
    public:
    static const int g_num = 128;
public:
    Epoll()
    {
        CreatEpoll();
    }
    void CreatEpoll()
    {
        _epfd = epoll_create(g_num);
        if(_epfd < 0) exit(5);
    }
    bool AddSockToEpoll(int sock, uint32_t events)
    {
        struct epoll_event ev;
        ev.data.fd = sock;
        ev.events = events;
        int n = epoll_ctl(_epfd, EPOLL_CTL_ADD, _epfd, &ev);
        return n == 0;
    }
    ~Epoll()
    {
    }

private:
    int _epfd;
};
