#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <unordered_map>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"

using namespace std;

class TcpServer;
class Connection;
using func_t = function<void(Connection *)>;

class Connection
{

public:
    Connection(int sock = -1)
        : _sock(sock),
          _tsvr(nullptr)
    {
    }
    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    int _sock; // 负责进行IO的文件描述符
    // 三个回调方法,表征的是对_sock进行特定读写对应的方法
    func_t _recv_cb;   // 读
    func_t _send_cb;   // 写
    func_t _except_cb; // 异常
    // 接收缓冲区 && 发送缓冲区
    string _inbuffer; // 暂时没有办法处理二进制流, 文本是可以的
    string _outbuffer;

    // 设置对TCPServer的回值指针
    TcpServer *_tsvr;
};

class TcpServer
{
    static const uint16_t g_port = 8080;

public:
    TcpServer(int port = g_port)
        : _port(port)
    {
        // 1. 创建listensock
        _listenSock = Sock::Socket();
        Sock::Bind(_listenSock, _port);
        Sock::Listen(_listenSock);

        // 2. 创建多路转接对象
        _poll.CreatEpoll();

        // 3. 添加listensock到服务器中
        AddConnection(_listenSock, Accepter, nullptr, nullptr);
    }

    // 专门针对任意sock进行添加TcpServer
    void AddConnection(int sock, func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        Sock::SetNonBlock(sock); // 设置非阻塞

        // 除了_listensock, 未来我们会存在大量的socket,每一个sock都必须被封装成一个Connection
        // 有大量的Connection时,TcpServer就需要将所有的Connection进行管理
        // 1. 构建conn对象,封装sock
        Connection *conn = new Connection(sock);
        conn->SetCallBack(recv_cb, send_cb, except_cb);
        conn->_tsvr = this;

        // 2. 添加sock[]到epoll中 
        // 任何多路转接服务器,一般只会默认打开读取事件的关心,写入事件会按需进行打开
        _poll.AddSockToEpoll(sock, EPOLLIN | EPOLLET);
        // 3. 还要将对应的Connection*对象指针添加到Connection映射表中
        _connections.insert(make_pair(sock, conn));
    }
    void Accepter()
    {

    }
    ~TcpServer()
    {
        if (_listenSock >= 0)
        {
            close(_listenSock);
        }
    }

private:
    int _listenSock;
    uint16_t _port;
    Epoll _poll;
    // sock : Connection
    unordered_map<int, Connection *> _connections;
};
