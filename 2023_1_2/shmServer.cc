#include "comm.hpp"

// 在加载的时候,会自动构建全局变量,就要调用构造函数 -> 创建管道
// 程序退出的时候,会调用析构函数 -> 删除管道文件
Init init;

string TransToHex(key_t k)
{
    char buffer[32];
    snprintf(buffer, sizeof buffer, "0x%x", k);
    return buffer;
}

int main()
{
    // 1. 创建公共的Key值
    key_t k = ftok(PATH_NAME, PROJ_ID);
    assert(k != -1);
    Log("create key done", Debug) << " server key : " << TransToHex(k) << endl;

    // 2. 创建共享内存 -- 创建全新的共享内存

    int shmid = shmget(k, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666); // 读写权限
    if (shmid == -1)
    {
        perror("shmget");
        exit(1);
    }

    Log("create shm done", Debug) << " shmid : " << shmid << endl;

    // sleep(10);

    // 3. 将指定的共享内存，挂接到自己的地址空间
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    Log("attach shm done", Debug) << " shmid : " << shmid << endl;
    // sleep(10);

    // 通信

    int fd = OpenFIFO(FIFO_NAME, READ);

    for (;;)
    {
        Wait(fd);

        printf("%s\n", shmaddr);
        if (strcmp(shmaddr, "quit") == 0)
            break;
        // sleep(1);
    }

    // while (true)
    // {
    //     ssize_t s= read(0, shmaddr, SHM_SIZE);
    //     if(s > 0)
    //     {
    //         shmaddr[s-1] = 0;

    //         if(strcmp(shmaddr,"quit") == 0) break;
    //     }
    // }

    // 4. 将指定的共享内存，从自己的地址空间中去关联

    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;

    Log("detach shm done", Debug) << " shmid : " << shmid << endl;

    // sleep(10);

    // 删除共享内存,IPC_RMID即便是有进程和当下的shm挂接，依旧删除共享内存

    n = shmctl(shmid, IPC_RMID, nullptr);
    assert(n != -1);
    (void)n;

    Log("delete shm done", Debug) << " shmid : " << shmid << endl;

    CloseFifo(fd);
    return 0;
}
