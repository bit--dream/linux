#ifndef _LOG_H_
#define _LOG_H_

#include <iostream>
#include <ctime>
#include <string>
using namespace std;

#define DEBUG   0
#define NOICE   1
#define WARNING 2
#define ERROR   3


const string msg[] = {"DEBUG", "NOICE", "WARNING", "ERROR"};

ostream &Log(string message, int level)
{
    cout << "|" << (unsigned)time(nullptr) << "|" << msg[level] << "|" << message;
    return cout;
}


#endif