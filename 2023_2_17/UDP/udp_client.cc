#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define SIZE 1024

static void usage(std::string proc)
{
    std::cout << "\nUsage: " << proc << "client_ip client_port\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage(argv[0]);
        exit(1);
    }
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        std::cerr << "socket error " << std::endl;
        exit(2);
    }
    // client 一般不会显示bind指定port,而是让OS自动随机选择
    std::string message;
    struct sockaddr_in server;
    memset(&server, 0, sizeof server);   
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(argv[2]));
    server.sin_addr.s_addr = inet_addr(argv[1]);

    char buffer[SIZE];
    while (true)
    {
        std::cout << "请输入你的信息# ";
        std::getline(std::cin, message);
        if(message == "quit") break;
        // 当client首次发送消息给服务器时,OS会自动给client bind一个IP和port
        sendto(sock, message.c_str(), sizeof message, 0, (struct sockaddr*) &server, sizeof server);
        
        struct sockaddr_in temp;
        socklen_t len = sizeof temp;
        ssize_t s = recvfrom(sock, buffer, sizeof buffer, 0, (struct sockaddr*)&temp, &len);
        if(s > 0)
        {
            buffer[s] = 0;
            std::cout << "server echo# " << buffer << std::endl;

        }
    }
    close(sock);
    return 0;
}