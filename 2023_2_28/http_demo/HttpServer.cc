#include <cassert>
#include <memory>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include "HttpServer.hpp"
#include "Usage.hpp"
#include "Util.hpp"
#include "Daemon.hpp"

#define ROOT "./wwwroot"
#define HOMEPAGE "index.html"

const int N = 10240;

void HandlerHttpRequest(int sockfd)
{
    // 1. 读取请求
    char buffer[N];
    ssize_t s = recv(sockfd, buffer, sizeof(buffer) - 1, 0);
    if (s > 0)
    {
        buffer[s] = 0;
        // std::cout << buffer << "------------------\n" << std::endl;
    }

    std::vector<std::string> vline;
    Util::CutString(buffer, "\r\n", &vline);

    std::vector<std::string> vt;
    Util::CutString(vline[0], " ", &vt);

    std::string file = vt[1];
    std::string target = ROOT;

    if (file == "/")
        file = "/index.html";

    target += file;
    std::cout << target << std::endl;

    std::string content;

    std::ifstream in(target); // 打开文件
    if (in.is_open())
    {
        std::string line;
        while (std::getline(in, line))
        {
            content += line;
        }
        in.close();
    }
    // 2. 构建一个http响应
    std::string http_response;
    if (content.empty())
        http_response = "HTTP/1.1 404 NotFound\r\n";
    else
        http_response = "HTTP/1.1 200 OK\r\n";

    http_response += "\r\n";
    http_response += content;

    send(sockfd, http_response.c_str(), http_response.size(), 0);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }
    // 守护进程
    MyDaemon();
    std::unique_ptr<HttpServer> http_server(new HttpServer(atoi(argv[1]), HandlerHttpRequest));

    http_server->Start();
    return 0;
}
