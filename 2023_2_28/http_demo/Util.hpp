#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <string>

class Util
{
public:
    // hhhhhh\r\naaaaaaaaa\r\nwwwwwwwwwww
    static void CutString(const std::string &str, const std::string &sep, std::vector<std::string> *out)
    {
        std::size_t start = 0;
        while (start < str.size())
        {

            auto pos = str.find(sep, start);
            if (pos == std::string::npos)
            {
                break;
            }
            else
            {
                std::string sub = str.substr(start, pos - start);
                // std::cout << "------" << sub << std::endl;
        
                out->push_back(sub);
                start += sub.size();
                start += sep.size();
            }
        }
        if(start < str.size())
        {
            out->push_back(str.substr(start));
        }
    }
};
