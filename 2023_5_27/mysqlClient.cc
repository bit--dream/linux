#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring>
#include "mysql.h"

using namespace std;

string host = "127.0.0.1";
string user = "dxl";
string passwd = "20030203";
string db = "dxl";
unsigned int port = 8080;

int main()
{
    // cout << "mysql client version: " << mysql_get_client_info() << endl;
    // 0. 初始化mysql对象
    MYSQL *mysql = mysql_init(nullptr);
    if (mysql == nullptr)
    {
        cout << "mysql_init error" << endl;
        exit(1);
    }

    // 1. 登录认证
    if (mysql_real_connect(mysql, host.c_str(), user.c_str(), passwd.c_str(), db.c_str(), port, nullptr, 0) == nullptr)
    {
        cerr << "mysql_real_connect error" << endl;
        exit(2);
    }

    mysql_set_character_set(mysql, "utf8");

    cout << "mysql_real_connect success" << endl;

    // string sql = "insert into emp values (666, '张飞', 783.12)";
    // string delSql = "delete from emp where id = 666";
    // string updateSql = "update emp set name='赵云' where id=666";
    // string selectSql = "select * from emp";
    
    char sql[1024];
    while (true)
    {
        printf("mysql> ");
        fgets(sql, sizeof sql, stdin);

        // 调用成功为0,失败为1
        int n = mysql_query(mysql, sql);

        if (strcasestr(sql, "select") && n == 0)
        {
            // 结果解析
            MYSQL_RES *res = mysql_store_result(mysql);
            if (res == nullptr)
                exit(0);

            int rows = mysql_num_rows(res);
            int fields = mysql_num_fields(res);

            MYSQL_FIELD *fname = mysql_fetch_fields(res);

            for (int i = 0; i < fields; i++)
                cout << fname[i].name << "\t|\t";
            cout << endl;

            MYSQL_ROW line;
            for (int i = 0; i < rows; i++)
            {
                line = mysql_fetch_row(res); // 按行获取文件的内容

                for (int j = 0; j < fields; j++)
                {
                    cout << line[j] << "\t|\t";
                }
                cout << endl;
            }

            printf("%d rows in set\n", rows);
        }
        else
        {
            cout << "execl sql : " << sql << " done" << endl;
        }
    }
    
    // 关闭数据库
    mysql_close(mysql);
    return 0;
}