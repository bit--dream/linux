#ifndef COMPILER_ONLINE
#include "header.cpp"

#endif
void Test1()
{
    vector<int> nums = {1,2,3,4,5,6}; 
    // 通过定义临时对象,来完成方法的调用
    int ret = Solution().Max(nums);
    if (ret == 6)
    {
        std::cout << "通过用例1, 测试用例是:{1,2,3,4,5,6}!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例1, 测试用例是:{1,2,3,4,5,6}" << std::endl;
    }
}

void Test2()
{
    vector<int> nums = {-1,-2,-3,-4,-5,-6}; 
    // 通过定义临时对象,来完成方法的调用
    int ret = Solution().Max(nums);
    if (ret == 6)
    {
        std::cout << "通过用例2,测试用例是:{-1,-2,-3,-4,-5,-6}!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例2, 测试用例是:{-1,-2,-3,-4,-5,-6}" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();
    return 0;
}