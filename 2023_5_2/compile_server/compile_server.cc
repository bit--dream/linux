#include "compile_run.hpp"
#include "../comm/httplib.h"

void Usage(std::string proc)
{
    std::cerr << "Usage : " << "\n\t" << proc << " port " << std::endl;
}

// 编译服务随时可能被多个人请求,必须保证传递上来的code,形成源文件名称的时候,要具有唯一性,不然多个用户会互相影响
//./compile_server port
int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        return 1;
    }
    httplib::Server svr;

    // svr.Get("/hello", [](const httplib::Request &req, httplib::Response &resp){
    //     // 基本测试
    //     resp.set_content("hello httplib,你好", "text/plain;charset=utf-8"); 
    // });

    svr.Post("/compile_and_run",[](const httplib::Request &req, httplib::Response &resp){
        // 用户请求的服务正文是我们想要的json string
        std::string in_json = req.body;
        std::string out_json;

        if(!in_json.empty())
        {
            my_compile_and_run::CompileAndRun::Start(in_json, &out_json);
            resp.set_content(out_json, "application/json;charset=uft-8");
        }
    });
    // svr.set_base_dir("./wwwroot");
    svr.listen("0.0.0.0", atoi(argv[1])); // 启动http服务
    return 0;
}
