#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <signal.h>
#include <jsoncpp/json/json.h>

int main()
{
    // 序列化
    // Value 是一个Json的中间类,可以填充KV值
    Json::Value root;
    root["code"] = "mycode";
    root["user"] = "dxl";
    root["age"] = "23";

    Json::StyledWriter writer;
    std::string str = writer.write(root);

    std::cout << str << std::endl;
    // 反序列化
    return 0;
}

// void handler(int signo)
// {
//     std::cout << "sino : " << signo << std::endl;
//     _exit(2);
// }

// int main()
// {
//     // 资源不足,OS发送信号终止进程
//     for(int i= 1; i <= 31;i++)
//     {
//         signal(i, handler);
//     }
//     // 限制累计运行时长
//     struct rlimit r;
//     r.rlim_cur = 1;
//     r.rlim_max = RLIM_INFINITY;
//     setrlimit(RLIMIT_CPU, &r);
//     while(1);

//     // struct rlimit rt;
//     // rt.rlim_cur = 1024 * 1024 * 40;
//     // rt.rlim_max = RLIM_INFINITY;
//     // setrlimit(RLIMIT_AS, &rt);
//     // int count = 0;
//     // while(true)
//     // {
//     //     int * p = new int [1024 * 1024];
//     //     count++;
//     //     std::cout << "size: " << count << std::endl;
//     //     sleep(1);
//     // }
//     return 0;
// }
