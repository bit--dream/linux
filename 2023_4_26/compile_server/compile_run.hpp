#pragma once
// 适配用户请求,定制通信协议字段
// 正确的调用compile and run
// 形成唯一的文件名

#include "compiler.hpp"
#include "runner.hpp"
#include <jsoncpp/json/json.h>

namespace my_compile_and_run
{
    class CompileAndRun
    {
    public:
        /*********************************
         * 输入:
         * code:用户提交的代码
         * input:用户给自己提交的代码对应的输入
         * 输出:
         * 
         * status:状态码
         * reason:请求结果
         * stdout:我的程序进行完的结果
         * stderr:我的程序运行完的错误结果
         * *******************************/
        static void Start(const std::string &in_json, std::string *out_json)
        {
            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value);
            std::string code = in_value["code"].asString();
            std::string input = in_value["input"].asString();

            if(code.size() == 0)
            {
                // 差错处理
            }
            
        }
    };
}
