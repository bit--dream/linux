#pragma once
#include <iostream>

namespace ns_protocol
{
    class Request
    {
    public:
        std::string Serialize()
        {
        }
        void Deserialize(const std::string &package)
        {
        }

    public:
        Request()
        {
        }

        Request(int x, int y, char op) : _x(x), _y(y), _op(op)
        {
        }

        ~Request() {}

    public:
        int _x;
        int _y;
        char _op;
    };

    class Response
    {
    public:
        std::string Serialize()
        {
        }
        void Deserialize(const std::string &package)
        {
        }

    public:
        Response()
        {
        }
        Response(int result, int code) : _result(result), _code(code)
        {
        }
        ~Response() {}

    public:
        int _result; // 计算结果
        int _code;   // 计算结果的状态码
    };

    std::string Recv(int sock)
    {
        char inbuffer[SIZE];
        ssize_t s = recv(sock, inbuffer, sizeof inbuffer, 0);
        if (s > 0)
        {
            return inbuffer;
        }
    }
    void Send(int sock, const std::string str)
    {
        send(sock, str.c_str(), str.size(), 0);
        
    }
}