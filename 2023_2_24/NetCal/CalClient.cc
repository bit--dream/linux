#include <iostream>

#include "Protocol.hpp"


static void Usage()
{
    std::cout << "./client port \n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage();
        exit(1);
    }

    return 0;
}