
#include <memory>

#include "TcpServer.hpp"
#include "Protocol.hpp"

static void Usage()
{
    std::cout << "./server port \n"
              << std::endl;
}

static ns_protocol::Response calculatorHelp(const ns_protocol::Request &req)
{
    ns_protocol::Response resp(0, 0);
    switch (req._op)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '/':
        if (0 == req._y)
            resp._code = 1;
        else
            resp._result = req._x / req._y;
        break;
    case '%':
        if (0 == req._y)
            resp._code = 2;
        else
            resp._result = req._x % req._y;
        break;
    default:
        resp._code = 3;
        break;
    }
    return resp;
}

void calculator(int sock)
{
    while (true)
    {
        std::string str = ns_protocol::Recv(sock);
        ns_protocol::Request req;
        req.Deserialize(str);

        ns_protocol::Response resp = calculatorHelp(req);  
        
        // 结构化 -> 序列化
        std::string respStr = resp.Serialize();
        ns_protocol::Send(sock, respStr); 

    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage();
        exit(1);
    }
    std::unique_ptr<ns_tcpserver::TcpServer> server(new ns_tcpserver::TcpServer(atoi(argv[1])));
    server->BindService(calculator);
    server->Start();

    return 0;
}