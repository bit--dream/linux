#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "ThreadPool/Log.hpp"

void usage()
{
    std::cout << "\nUsage: "
              << ".\tcp_client"
              << " serverIp serverPort\n"
              << std::endl;
}

// ./tcp_client targetIp targetPort
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage();
        exit(1);
    }

    std::string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);
    int sock = 0;

    bool alive = false;
    std::string line;

    while (true)
    {
        if (!alive)
        {
            sock = socket(AF_INET, SOCK_STREAM, 0);
            if (sock < 0)
            {
                std::cerr << "socket error" << std::endl;
                exit(2);
            }

            // client 需要让os自动进行port选择

            struct sockaddr_in server;
            memset(&server, 0, sizeof(server));
            server.sin_family = AF_INET;
            server.sin_port = htons(serverport);
            server.sin_addr.s_addr = inet_addr(serverip.c_str());

            if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
            {
                std::cerr << "connect error" << std::endl;
                exit(3);
            }
            else
            {
                std::cout << "connect success" << std::endl;
            }
            alive = true;
        }

        std::cout << "请输入# ";
        std::getline(std::cin, line);
        if (line == "quit")
            break;

        ssize_t s = send(sock, line.c_str(), line.size(), 0);
        if (s > 0)
        {
            char buffer[1024];
            ssize_t r = recv(sock, buffer, sizeof(buffer) - 1, 0);
            if (r > 0)
            {
                buffer[r] = 0;
                std::cout << "server 回显# " << buffer << std::endl;
            }
            else if (r == 0)
            {

                close(sock);
                std::cout << "回显失败" << std::endl;
            }
        }
        alive = false;
    }
    close(sock);
    return 0;
}