#include "TcpServer.hpp"
#include <memory>

static Response Calculator(const Request &req)
{
    Response resp(0, 0, req._x, req._op, req._y);
    switch (req._op)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '/':
        if (0 == req._y)
            resp._code = 1;
        else
            resp._result = req._x / req._y;
        break;
    case '%':
        if (0 == req._y)
            resp._code = 2;
        else
            resp._result = req._x % req._y;
        break;
    default:
        resp._code = 3;
        break;
    }
    return resp;
}



void NetCal(Connection *conn, std::string &request)
{
    logMessage(DEBUG, "NetCal been called, get request: %s", request.c_str());
    // 1. 反序列化
    Request req;
    if(!req.Deserialize(request)) return;

    // 2. 业务处理
    Response resp = Calculator(req);

    // 3. 序列化,构建应答
    string sendstr = resp.Serialize();
    sendstr = Encode(sendstr);
    
    // 4. 交给服务器
    conn->_outbuffer += sendstr;

    // 5. 想办法让底层TcpServer发送数据
    // a. 完整的发送逻辑
    // b. 我们一旦开启EPOLLOUT, epoll会自动立马触发一次发送事件就绪,如果后续保持发送的开启,epoll会一直发送
    conn->_tsvr->EnableReadWrite(conn, true, true);

}


int main()
{
    std::unique_ptr<TcpServer> svr(new TcpServer());
    svr->Dispatcher(NetCal);
    return 0;
}