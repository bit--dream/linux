#pragma once
#include <iostream>
#include <cstring>
#include <vector>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>

// 解决粘包问题,处理独立报文
#define SEP "X"
#define SEP_LEN strlen(SEP)
// 自己手写序列反序列化
#define SPACE " "
#define SPACE_LEN strlen(SPACE)

// 将传入的缓冲区进行切分
// 1. buffer被切走的要从buffer中移除

void SpliteMessage(std::string &buffer, std::vector<std::string> *out)
{
    // 100+19X100+19
    while (true)
    {
        auto pos = buffer.find(SEP);
        if (std::string::npos == pos)
            break;
        std::string message = buffer.substr(0, pos);
        buffer.erase(0, pos + SEP_LEN);
        out->push_back(message);
    }
}

// 添加报头长度
// 
std::string Encode(std::string &s)
{
    return s + SEP;
}


class Request
{
public:
    // "length\r\n_x _op _y\r\nlength\r\n_x _op _y\r\nlength\r\n_x _op _y\r\n"
    //
    std::string Serialize() // 序列化
    {
        std::string str;
        str = std::to_string(_x);
        str += SPACE;
        str += _op;
        str += SPACE;
        str += std::to_string(_y);

        return str;
    }

    bool Deserialize(const std::string &package) //  反序列化
    {
        std::size_t left = package.find(SPACE);
        if (left == std::string::npos)
            return false;
        std::size_t right = package.rfind(SPACE);
        if (left == std::string::npos)
            return false;

        _x = atoi(package.substr(0, left).c_str());
        if (left + SPACE_LEN > package.size())
            return false;
        else
            _op = package[left + SPACE_LEN];
        _y = atoi(package.substr(right + SPACE_LEN).c_str());

        return true;
    }

public:
    Request()
    {
    }

    Request(int x, int y, char op) : _x(x), _y(y), _op(op)
    {
    }

    ~Request() {}

public:
    int _x;
    int _y;
    char _op;
};

class Response
{
public:
    std::string Serialize()
    {
        std::string s;
        s = std::to_string(_code);
        s += SPACE;
        s += std::to_string(_result);

        return s;
    }

    bool Deserialize(const std::string &package)
    {

        std::size_t pos = package.find(SPACE);
        if (pos == std::string::npos)
            return false;

        _code = atoi(package.substr(0, pos).c_str());
        _result = atoi(package.substr(pos + SPACE_LEN).c_str());

        return true;
    }

public:
    Response()
    {
    }
    Response(int result, int code, int x, char op, int y)
        : _result(result),
          _code(code),
          _x(x),
          _op(op),
          _y(y)
    {
    }
    ~Response() {}

public:
    int _x;
    int _y;
    char _op;
    int _result; // 计算结果
    int _code;   // 计算结果的状态码
};