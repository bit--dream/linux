#pragma once

#include <iostream>
#include <functional>
#include <string>
#include <vector>
#include <unordered_map>
#include <cerrno>
#include <cassert>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"
#include "Protocol.hpp"

class TcpServer;
class Connection;

using func_t = std::function<void(Connection *)>;
using work_t = std::function<void(Connection *, std::string &request)>;

// 为了能够正常工作,常规的sock必须要有自己独立的接收缓冲区&&发送缓冲区
class Connection
{

public:
    Connection(int sock = -1)
        : _sock(sock), _tsvr(nullptr)
    {
    }

    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    int _sock; // 负责进行IO的文件描述符

    func_t _recv_cb;   // 读回调
    func_t _send_cb;   // 写回调
    func_t _except_cb; // 异常回调

    std::string _inbuffer; // 暂时没有办法处理二进制流,文本是可以的
    std::string _outbuffer;

    TcpServer *_tsvr; // 对TcpServer的回值指针

    uint64_t _lasttimestamp;
};

class TcpServer
{
    const static uint16_t gport = 8080;
    const static uint16_t gnum = 128;

public:
    TcpServer(uint16_t port = gport)
        : _port(port), _revs_num(gnum)
    {
        // 1. 创建listensock
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 2. 创建epoll对象
        _poll.CreateEpoll();

        // 3. 添加listensock到服务器中
        AddConnection(_listensock, std::bind(&TcpServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);

        // 4.构建一个获取就绪事件的缓冲区
        _revs = new struct epoll_event[_revs_num];
    }

    // 专门针对任意sock进行添加TcpServer
    void AddConnection(int sock, func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        Sock::SetNonBlock(sock);
        // 1.构建Connection对象, 封装sock
        Connection *conn = new Connection(sock);
        conn->SetCallBack(recv_cb, send_cb, except_cb);
        conn->_tsvr = this;
        conn->_lasttimestamp = time(nullptr);
        // 2. 添加sock到epoll模型
        _poll.AddSockToEpoll(sock, EPOLLIN | EPOLLET); // 任何多路转接的服务器,一般默认只会打开对读取事件的关心,写入事件按需打开

        // 3.将对应的Connection*对象指针添加到Connections集合中
        _connections.insert(std::make_pair(sock, conn));
    }

    void Accepter(Connection *conn)
    {
        // logMessage(DEBUG, "Accepter been called");
        // 一定有listensock已经就绪,此次读取不会阻塞
        // 要保证底层的链接全部都被取走了
        while (true)
        {
            std::string clientip;
            uint16_t clientport;
            int accept_errno = 0;
            int sock = Sock::Accept(conn->_sock, &clientport, &clientip, &accept_errno);
            if (sock < 0)
            {
                if (accept_errno == EAGAIN || accept_errno == EWOULDBLOCK)
                    break;
                else if (accept_errno == EINTR) // 信号中断
                    continue;
                else
                {
                    // accept失败
                    logMessage(WARNING, "accept error, %d : %s", accept_errno, strerror(accept_errno));
                    break;
                }
            }
            if (sock >= 0)
            {
                // 将sock托管给TcpServer
                AddConnection(sock, std::bind(&TcpServer::Recver, this, std::placeholders::_1),
                              std::bind(&TcpServer::Sender, this, std::placeholders::_1),
                              std::bind(&TcpServer::Excepter, this, std::placeholders::_1));

                logMessage(DEBUG, "accept client %s:%d success, add to epoll && TcpServer success, sock:%d",
                           clientip.c_str(), clientport, sock);
            }
        }
    }
    // 使一个文件能开启读写
    void EnableReadWrite(Connection *conn, bool readable, bool writeable)
    {
        uint32_t events = ((readable ? EPOLLIN : 0) | (writeable ? EPOLLOUT : 0));
        bool res = _poll.CtrlEpoll(conn->_sock, events);
        assert(res);
    }

    void Recver(Connection *conn)
    {
        conn->_lasttimestamp = time(nullptr); // 更新最近访问时间
        const int num = 1024;

        bool err = false;
        // logMessage(DEBUG, "Recver event exists, Recver() been called");
        // 面向字节流,进行常规读取
        cout << "Recver success" << endl;
        while (true)
        {
            char buffer[num];
            ssize_t n = recv(conn->_sock, buffer, sizeof(buffer) - 1, 0);
            if (n < 0)
            {
                cout << "读取失败" << endl;
                if (errno == EAGAIN || errno == EWOULDBLOCK) //  正常结束
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    logMessage(ERROR, "recv error, %d : %s", errno, strerror(errno));
                    conn->_except_cb(conn);
                    err = true;
                    break;
                }
            }
            else if (n == 0)
            {
                logMessage(DEBUG, "client[%d] quit, server close", conn->_sock);
                conn->_except_cb(conn);
                err = true;
                break;
            }
            else
            {
                // 读取成功
                buffer[n] = 0;
                conn->_inbuffer += buffer;
                cout << "读取成功" << endl;
            }
        }
        logMessage(DEBUG, "conn->_inbuffer[sock : %d]: %s", conn->_sock, conn->_inbuffer.c_str());
        if (!err)
        {
            vector<string> messages;
            SpliteMessage(conn->_inbuffer, &messages);
            for (auto &msg : messages)
                _wt(conn, msg);
        }
    }

    void Sender(Connection *conn)
    {
        while (true)
        {
            ssize_t n = send(conn->_sock, conn->_outbuffer.c_str(), conn->_outbuffer.size(), 0);
            if (n > 0)
            {
                conn->_outbuffer.erase(0, n);
                if (conn->_outbuffer.empty())
                    break;
            }
            else
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    logMessage(ERROR, "send error, %d : %s", errno, strerror(errno));
                    conn->_except_cb(conn);
                    break;
                }
            }
        }
        // 如果没有出错,一定是要么发完,要么发送条件不满足,下次再发送
        if (conn->_outbuffer.empty())
            EnableReadWrite(conn, true, false);
        else
            EnableReadWrite(conn, true, true);
    }

    void Excepter(Connection *conn)
    {
        if (!IsConnectionExists(conn->_sock))
            return;
        // 1. 从epoll中移除
        if (!_poll.DelFromEpoll(conn->_sock))
            assert(1);
        // 2. 从unordered_map中移除
        _connections.erase(conn->_sock);
        // 3. close(sock)
        close(conn->_sock);
        // 4. delete conn
        delete conn;

        logMessage(DEBUG, "Excepter 回收完毕所有的异常情况");
    }

    bool IsConnectionExists(int sock)
    {
        auto iter = _connections.find(sock);
        if (iter == _connections.end())
            return false;
        return true;
    }
    void LoopOnce()
    {
        int n = _poll.WaitEpoll(_revs, _revs_num);
        for (int i = 0; i < n; i++)
        {
            int sock = _revs[i].data.fd;
            uint32_t revents = _revs[i].events;

            // 将所有的异常,全部交给read或者write来统一处理
            if (revents & EPOLLERR)
                revents |= (EPOLLIN | EPOLLOUT);
            if (revents & EPOLLHUP)
                revents |= (EPOLLIN | EPOLLOUT);
            
            if (revents & EPOLLIN)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_recv_cb)
                {
                    _connections[sock]->_recv_cb(_connections[sock]);
                }
            }
            if (revents & EPOLLOUT)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_send_cb)
                {
                    _connections[sock]->_send_cb(_connections[sock]);
                }
            }
        }
    }

    void ConnectAliveCheck()
    {
        for(auto &x : _connections)
        {
            uint64_t currtime = time(nullptr);
            if(currtime - x.second->_lasttimestamp >= 5000 * 60)
            {
                logMessage(DEBUG, "client[%d] has not been connected for a long time", x.first);
            }
        }
    }
    
    // 根据就绪事件,进行特定事件的派发
    void Dispatcher(work_t wt)
    {
        _wt = wt;
        while (true)
        {
            ConnectAliveCheck();
            LoopOnce();

        }
    }

    ~TcpServer()
    {
        if (_listensock >= 0)
            close(_listensock);
        if (_revs)
            delete[] _revs;
    }

private:
    int _listensock;
    uint16_t _port;
    Epoll _poll;
    // sock : connection
    std::unordered_map<int, Connection *> _connections;
    struct epoll_event *_revs;
    int _revs_num;

    // 上层业务处理
    work_t _wt;
};
