#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#define NUM 1024

struct _MyFILE
{
    int fd;
    char buffer[NUM];
    int end;//当前缓冲区的结尾
};

typedef struct _MyFILE MyFILE;

MyFILE* _fopen(const char* pathname, const char* mode)
{
    assert(pathname);
    assert(mode);
    
    MyFILE* fp = NULL; 

    if(strcmp(mode, "r") == 0)
    {

    }
    else if (strcmp(mode, "r+") == 0)
    {

    }
    else if (strcmp(mode, "w") == 0)
    {
        int fd = open(pathname, O_WRONLY | O_TRUNC | O_CREAT, 0666);

        if(fd >= 0)
        {
            fp = (MyFILE*)malloc(sizeof(MyFILE));
            memset(fp, 0, sizeof(MyFILE));
            fp->fd = fd;
        }
    }
    else if (strcmp(mode, "w+") == 0)
    {

    }
    else if (strcmp(mode, "a") == 0)
    {

    }
    else if (strcmp(mode, "a+") == 0)
    {

    }
    else 
    {

    }

    return fp;
}
void _fputs(const char* message, MyFILE* fp)
{
    assert(message);
    assert(fp);
    strcpy(fp->buffer + fp->end, message);
    fp->end += strlen(message);
    
    printf("%s\n", fp->buffer);

    //用户通过执行C标准库中的代码逻辑，来完成刷新动作    
    //这里效率提高，体现在哪里呢？？因为C提供了缓冲区，那么我们就通过策略，减少了IO的执行次数(不是数据量)
    if(fp->fd == 0)
    {
        //标准输入
    }
    else if (fp->fd == 1)
    {
        //标准输出
        if(fp->buffer[fp->end-1] == '\n')
        {
            fprintf(stderr, "fflush: %s", fp->buffer);
            write(fp->fd, fp->buffer, fp->end);
            fp->end = 0;

        }
    }
    else if (fp->fd == 2)
    {
        //标准错误
    }
    else 
    {
        //其他文件
        
    }
}


void _fflush(MyFILE* fp)
{
    assert(fp);
    if(fp->end != 0)
    {
        write(fp->fd, fp->buffer, fp->end);
        syncfs(fp->fd);//将数据从内核写到磁盘
        fp->end = 0;
    }
}

void _fclose(MyFILE* fp)
{
    
    assert(fp);
    _fflush(fp);
    close(fp->fd);
    free(fp);
}

int main()
{

    close(1);
    MyFILE* fp = _fopen("./log.txt", "w");
    if(fp == NULL)
    {
        printf("open file error\n");
        return 1;
    }

    _fputs("one:hello world", fp);
    sleep(1);
    _fputs(" two:hello world\n", fp);
    sleep(1);
    _fputs(" three:hello world\n", fp);
    sleep(1);
    _fputs(" four:hello world", fp);
    sleep(1);
    _fputs(" five:hello world\n", fp);
    sleep(1);

    _fclose(fp);

    return 0;
}

//int main()
//{
//    //C语言提供
//    printf("hello printf\n");
//    fprintf(stdout, "hello fprintf\n");
//    const char* s = "hello fputs\n";
//    fputs(s, stdout);
//
//    //OS提供
//    const char* ss = "hello write\n";
//    write(1, ss, strlen(ss));
//
//    fork();//创建子进程
//
//
//
//    return 0;
//}
//
////int main(int argc, char* argv[])
////{
////    if(argc != 2)
////    {
////        return 2;
////    }
////    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC);
////    if(fd < 0)
////    {
////        perror("open");
////        return 1;
////    }
////    dup2(fd, 1);
////    fprintf(stdout, "%s\n", argv[1]);
////
////
////
////
////
////
////
////
////
////
////    //close(0);
////    //int fd = open("log.txt", O_RDONLY);
////
////    //if(fd < 0)
////    //{
////    //     perror("open");
////    //     return 1;
////    //}
////    //printf("fd:%d\n", fd);
////    //
////    //char buffer[64];
////    //fgets(buffer, sizeof buffer, stdin);
////
////    //printf("%s\n", buffer);
////
////   // close(1);
////   // // 这里fd的分配规则：最小的，没有被占用的文件描述符
////   // int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
////
////   // if(fd < 0)
////   // {
////   //     perror("open");
////   //     return 1;
////   // }
////
////   // printf("fd:%d\n", fd);
////   // printf("fd:%d\n", fd);
////   // printf("fd:%d\n", fd);
////   // printf("fd:%d\n", fd);
////   // printf("fd:%d\n", fd);
////   // fprintf(stdout, "hello fprintf\n");
////   // const char* s = "hello fwrite\n";
////   // fwrite(s, strlen(s), 1, stdout);
////
////   // fflush(stdout);
////   // close(fd);
////   // return 0;
////}
////
