#include <iostream>
#include "Sock.hpp"
#include "Protocol.hpp"

using namespace ns_protocol;

static void Usage()
{
    std::cout << "\nUsage: "
              << "./client serverIp serverPort\n"
              << std::endl;
}

// ./client server_ip server_port
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage();
        exit(1);
    }
    std::string server_ip = argv[1];
    uint16_t server_port = atoi(argv[2]);

    Sock sock;
    int sockfd = sock.Socket();
    if (!sock.Connect(sockfd, server_ip, server_port))
    {
        std::cerr << "Connect error" << std::endl;
        exit(2);
    }

    bool quit = false;
    std::string buffer;
    while (!quit)
    {
        // 1.获取需求
        ns_protocol::Request req;
        std::cout << "请输入计算表达式:>";
        std::cin >> req._x >> req._op >> req._y;

        // 2.序列化
        std::string s = req.Serialize();
        // std::string temp = s;

        // 3.添加长度报头
        s = ns_protocol::Encode(s);

        // 4. 发送到服务端
        Send(sockfd, s);

        // 5. 读取
        while (true)
        {
            bool res = Recv(sockfd, &buffer);
            if (!res)
            {
                quit = true;
                break;
            }
            //
            std::string package = ns_protocol::Decode(buffer);
            if (package.empty())
                continue;

            ns_protocol::Response resp;
            resp.Deserialize(package);
            std::string err;
            switch (resp._code)
            {
            case 1:
                err = "除0错误";
                break;
            case 2:
                err = "模0错误";
                break;
            case 3:
                err = "非法操作";
                break;
            default:
                std::cout << resp._x << resp._op << resp._y << " = " << resp._result << " [success] " << std::endl;
                break;
            }
            if (!err.empty())
                std::cout << err << std::endl;
            // std::cout << "code: " << resp._code << " | "  << "result: " << resp._result << std::endl;
            break;
        }
    }
    return 0;
}