#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>

int main()
{
    Json::Value root;
    root["a"] = 10;
    root["b"] = 10;
    root["op"] = '*';

    Json::Value sub;
    sub["other1"] = 2000;
    sub["other2"] = "qwwea";
    
    root["sub"] = sub;
    // Json::StyledWriter writer;
    Json::FastWriter writer;
    std::string str = writer.write(root);
    std::cout << str << std::endl;

    return 0;
}

