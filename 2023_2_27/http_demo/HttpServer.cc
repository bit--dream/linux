#include <cassert>
#include <memory>
#include "HttpServer.hpp"
#include "Usage.hpp"

#define ROOT "./wwwroot"
const int N = 10240;

void HandlerHttpRequest(int sockfd)
{
    // 1. 读取请求

    char buffer[N];
    ssize_t s = recv(sockfd, buffer, sizeof(buffer) - 1, 0);
    if (s > 0)
    {
        buffer[s] = 0;
        std::cout << buffer << "------------------\n"
                  << std::endl;
    }
    // 2. 构建一个http响应
    std::string http_response = "HTTP/1.1 200 OK\r\n";
    http_response += "\r\n";
    http_response += "<html><h2> </h2></html>";
    send(sockfd, http_response.c_str(), http_response.size(), 0);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }
    std::unique_ptr<HttpServer> http_server(new HttpServer(atoi(argv[1]), HandlerHttpRequest));

    http_server->Start();
    return 0;
}
