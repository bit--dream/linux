#pragma once
#include <iostream>
#include <string>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <algorithm>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "Log.hpp"

const int SIZE = 1024;

static void service(int sock, const std::string &client_ip, const uint16_t &client_port)
{
    // echo server
    char buffer[SIZE];
    while (true)
    {
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = 0;
            logMessage(NORMAL, "%s: %d ## %s\n", client_ip.c_str(), client_port, buffer);
        }
        else if (s == 0) // 对端关闭连接
        {
            logMessage(NORMAL, "%s : %d shutdown, me too!", client_ip.c_str(), client_port);
            break;
        }
        else
        {
            logMessage(ERROR, "read socket error, %d : %s", errno, strerror(errno));
            break;
        }

        write(sock, buffer, strlen(buffer));
    }
}




class TcpServer
{
private:
    const static int gbacklog = 20;

public:
    TcpServer(uint16_t port, std::string ip = "0.0.0.0")
        : _port(port),
          _ip(ip),
          _listenSocket(-1)
    {
    }  
    void initServer()
    {
        // 1.创建套接字
        _listenSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (_listenSocket < 0)
        {
            logMessage(FATAL, "create socket error, %d : %s", errno, strerror(errno));
            exit(2);
        }
        logMessage(NORMAL, "create socket success, _listenSocket: %d", _listenSocket); // 3
        
        // 2. bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        local.sin_port = htons(_port);

        if (bind(_listenSocket, (struct sockaddr *)&local, sizeof local) < 0)
        {
            logMessage(FATAL, "bind error, %d : %s", errno, strerror(errno));
            exit(3);
        }
        // 建立连接
        if (listen(_listenSocket, gbacklog) < 0)
        {
            logMessage(FATAL, "listen error, %d : %s", errno, strerror(errno));
            exit(4);
        }
        logMessage(NORMAL, "init server success!\n");
    }

    void start()
    {
        // signal(SIGCHLD, SIG_IGN); // 主动忽略

        while (true)
        {
            // sleep(1);
            struct sockaddr_in temp;
            socklen_t len = sizeof temp;
            // 提供服务                 获取新建连接
            int service_socket = accept(_listenSocket, (struct sockaddr *)&temp, &len);
            if (service_socket < 0)
            {
                logMessage(ERROR, "accept error, %d : %s", errno, strerror(errno));
                continue;
            }
            //  连接成功
            uint16_t client_port = ntohs(temp.sin_port);
            std::string client_ip = inet_ntoa(temp.sin_addr);
            logMessage(NORMAL, "link success, service_socket : %d | %s : %d\n", service_socket, client_ip.c_str(), client_port);

            // 开始通信
            // version 2.1 -- 多进程 
            pid_t id = fork();
            if(id == 0)
            {
                // child
                if(fork() > 0) exit(0);
                
                                
            }
            // parent

            // version 1 -- 单进程循环 -- 只能够进行一次处理一个客户端. 处理完了一个, 才能处理下一下
            //
            // service(service_socket, client_ip, client_port);

            // version 2.0 -- 多进程版 -- 创建子进程 ,让子进程给新的连接提供服务,子进程可以使用父进程曾经打开的文件fd
            // pid_t id = fork();
            // assert(id != -1);
            // if (id == 0)
            // {
            //     // 子进程 是进行提供服务的,不需要知道监听socket
            //     close(_listenSocket);
            //     service(service_socket, client_ip, client_port);
            //     exit(0); // 僵尸进程
            // }
            // // 父进程
            // close(service_socket);
        }
    }
    ~TcpServer() {}

private:
    uint16_t _port;
    std::string _ip;
    int _listenSocket;
};
