#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "oj_model.hpp"
#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "oj_view.hpp"

namespace my_control
{
    class Control
    {
    private:
        my_model::Model _model; // 提供后台数据
        my_view::View _view;    // 提供html渲染功能

    public:
        Control()
        {
        }
        ~Control()
        {
        }

        // 根据题目数据构建网页
        // html 输出型参数
        bool AllQuestions(std::string *html)
        {
            bool ret = true;
            std::vector<my_model::Question> all;
            if (_model.GetAllquestions(&all))
            {
                // 获取题目信息成功,将所有的题目数据构建成网页
                _view.AllExpandHtml(all, html);
            }
            else
            {
                *html = "获取题目列表失败,形成题目列表失败";
                ret = false;
            }
            return ret;
        }

        bool Question(std::string number, std::string *html)
        {
            bool ret = true;
            my_model::Question q;
            if (_model.GetO0neQuestions(number, &q))
            {
                // 获取指定题目信息成功,将题目数据构建成网页
                _view.OneExpandHtml(q, html);
            }
            else
            {
                *html = "指定题目: " + number + " 不存在!";
                ret = false;
            }
            return ret;
        }
    };
}