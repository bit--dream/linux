#pragma once

#include <iostream>
#include <string>
#include <ctemplate/template.h>

#include "oj_model.hpp"

namespace my_view
{
    class View
    {
    private:
        View()
        {
        }
        ~View()
        {
        }

    public:
        void AllExpandHtml(const std::vector<my_model::Question> &questions, std::string *html)
        {
        }
        void OneExpandHtml(const my_model::Question &q, std::string *html)
        {
        }
    };
}
