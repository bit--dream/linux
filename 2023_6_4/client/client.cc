#include <iostream>
#include <fstream>
#include "contacts.pb.h"
using namespace std;
using namespace c_contacts;
using namespace google::protobuf;
/**
 * 打印联系⼈列表
 */
void PrintfContacts(const Contacts &contacts)
{
    for (int i = 0; i < contacts.contacts_size(); ++i)
    {
        const PeopleInfo &people = contacts.contacts(i);
        cout << "------------联系⼈" << i + 1 << "------------" << endl;
        cout << "姓名：" << people.name() << endl;
        cout << "年龄：" << people.age() << endl;
        int j = 1;
        for (const PeopleInfo_Phone &phone : people.phone())
        {
            cout << "电话" << j++ << ": " << phone.number() << endl;
        }

        const Reflection *reflection = PeopleInfo::GetReflection();
        const UnknownFieldSet &set = reflection->GetUnknownFields(people);
        for (int j = 0; j < set.field_count(); j++)
        {
            const UnknownField &unknownfield = set.field(j);
            cout << "未知字段" << j + 1 << " : "
                 << " 编号 : " << unknownfield.number();
            switch (unknownfield.type())
            {
            case UnknownField::Type::TYPE_VARINT:
                cout << " 值: " << unknownfield.varint() << endl;
                break;
            case UnknownField::Type::TYPE_LENGTH_DELIMITED:
                cout << " 值: " << unknownfield.length_delimited() << endl;
                break;
            } 
        }
    }
}

int main()
{
    // 以⼆进制⽅式读取 contacts
    Contacts contacts;
    fstream input("../service/contacts.bin", ios::in | ios::binary);
    if (!contacts.ParseFromIstream(&input))
    {
        cerr << "Failed to parse contacts." << endl;
        input.close();
        return -1;
    }
    // 打印 contacts
    PrintfContacts(contacts);
    input.close();
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
