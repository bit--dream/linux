#pragma once
#include <iostream>
#include <cstring>
#include <vector>
#include <unistd.h>
// 1. 报文之间采用特殊字符来进行解决粘包问题
// 2. 获取一个一个独立完整的报文,序列化和反序列化

#define SEP "X"
#define SEP_LEN strlen(SEP)

#define SPACE " "
#define SPACE_LEN strlen(SPACE)

class Request
{
public:
    Request()
    {
    }
    Request(int x, int y, char op)
        : _x(x),
          _y(y),
          _op(op)
    {
    }
    ~Request()
    {
    }
    std::string Serialize()
    {
        std::string str;
        str = std::to_string(_x);
        str += SPACE;
        str += _op;
        str += SPACE;
        str += std::to_string(_y);
        return str;
    }
    bool Deserialize(const std::string &str)
    {
        std::size_t left = str.find(SPACE);
        if (left == std::string::npos)
            return false;
        std::size_t right = str.rfind(SPACE);
        if (right == std::string::npos)
            return false;
        _x = atoi(str.substr(0, left).c_str());
        _y = atoi(str.substr(right + SPACE_LEN).c_str());
        if (left + SPACE_LEN > str.size())
            return false;
        else
            _op = str[left + SPACE_LEN];
        return true;
    }

public:
    int _x;
    int _y;
    char _op;
};

class Response
{
public:
    Response()
    {
    }
    Response(int result, int code)
        : _result(result),
          _code(code)
    {
    }
    ~Response()
    {
    }

    std::string Serialize()
    {
        std::string s;
        s = std::to_string(_code);
        s += SPACE;
        s += std::to_string(_result);
        return s;
    }
    bool Deserialize(const std::string &s)
    {
        auto pos = s.find(SPACE);
        if (pos == std::string::npos)
            return false;
        _code = atoi(s.substr(0, pos).c_str());
        _result = atoi(s.substr(pos + SPACE_LEN).c_str());
        return true;
    }

public:
    int _result;
    int _code;

};

// 将传入的缓冲区进行切分
// 1. 被切走的部分, 也要在buffer中切除
// 2. 可能存在多个报文, 依次放入out中
// buffer 输入输出型参数
// out 输出型参数
void SpliteMessage(std::string &buffer, std::vector<std::string> *out)
{
    while (true)
    {
        auto pos = buffer.find(SEP);
        if (std::string::npos == pos)
            break;
        std::string message = buffer.substr(0, pos); // 左闭右开
        buffer.erase(0, pos + SEP_LEN);
        out->emplace_back(message);
        // std::cout << "DEBUG" << message << " : " << buffer << std::endl;
        // sleep(1);
    }
}

std::string Encode(std::string &s)
{
    return s + SEP;
}
