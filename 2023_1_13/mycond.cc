#include <iostream>
#include <string>
#include <unistd.h>
#include <pthread.h>

using namespace std;

#define THREAD_NUM 4

typedef void (*func_t)(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcnt);
volatile bool quit = false;

// pthread_cond_t cnt = PTHREAD_COND_INITIALIZER;
// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

class ThreadData
{
public:
    ThreadData(const string &name, func_t func, pthread_mutex_t *pmtx, pthread_cond_t *pcnt)
        : _name(name), _func(func), _pmtx(pmtx), _pcnt(pcnt)
    {
    }

public:
    string _name;
    func_t _func;
    pthread_mutex_t *_pmtx;
    pthread_cond_t *_pcnt;
};

void func1(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcnt)
{
    while (!quit)
    {
        // wait一定要在加锁和解锁之间进行wait
        pthread_mutex_lock(pmtx);
        // 临界资源是否就绪
        pthread_cond_wait(pcnt, pmtx); // 默认该线程在执行的时候,wait被执行,当前线程立即会被阻塞
        cout << name << " running... a" << endl;
        pthread_mutex_unlock(pmtx);
        // sleep(1);
    }
}

void func2(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcnt)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcnt, pmtx);
        cout << name << " running... b" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void func3(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcnt)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcnt, pmtx);
        cout << name << " running... c" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void func4(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcnt)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcnt, pmtx);
        cout << name << " running... d" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void *Entry(void *args)
{
    ThreadData *td = (ThreadData *)args; // td在每一个线程自己私有的栈空间中保存
    td->_func(td->_name, td->_pmtx, td->_pcnt);
    delete td;
    return nullptr;
}

int main()
{
    pthread_mutex_t mtx;
    pthread_cond_t cnt;
    pthread_mutex_init(&mtx, nullptr);
    pthread_cond_init(&cnt, nullptr);

    pthread_t tids[THREAD_NUM];
    func_t funcs[THREAD_NUM] = {func1, func2, func3, func4};

    for (int i = 0; i < THREAD_NUM; i++)
    {
        string name = "Thread ";
        name += to_string(i + 1);
        ThreadData *td = new ThreadData(name, funcs[i], &mtx, &cnt);
        pthread_create(&tids[i], nullptr, Entry, (void *)td);
    }

    sleep(5);

    int n = 5;
    // 唤醒
    while (n--)
    {
        cout << "resume thread run code ... " << n << endl;
        // pthread_cond_signal(&cnt);
        pthread_cond_broadcast(&cnt);
        sleep(1);
    }

    quit = true;
    cout << "ctrl done" << endl;
    pthread_cond_broadcast(&cnt);

    for (int i = 0; i < THREAD_NUM; i++)
    {
        pthread_join(tids[i], nullptr);
        cout << "thread: " << tids[i] << " quit" << endl;
    }

    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cnt);
    return 0;
}
