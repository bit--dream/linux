#pragma once

#include <iostream>
#include <signal.h>
#include <functional>
#include "Sock.hpp"

class HttpServer
{
public:
    using func_t = std::function<void(int)>;

public:
    HttpServer(const uint16_t &port, func_t func) : _port(port), _func(func)
    {
        _listensock = _sock.Socket();
        _sock.Bind(_listensock, port);
        _sock.Listen(_listensock);

    }

    void Start()
    {
        signal(SIGCHLD, SIG_IGN);
        for (;;)
        {
            std::string client_ip;
            uint16_t client_port = 0;
            int sockfd = _sock.Accept(_listensock, &client_port, &client_ip);
            if (sockfd < 0)
                continue;
            if (fork() == 0)
            {
                close(_listensock);
                _func(sockfd);
                close(sockfd);
                exit(0);
            }
            close(sockfd);
        }
    }

    ~HttpServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    Sock _sock;
    int _listensock;
    uint16_t _port;
    func_t _func;
};
