#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "Task.hpp"

using namespace std;
#define PROCESS_NUM 5


int waitCommand(int waitFd, bool &quit)
{
    uint32_t command = 0;
    ssize_t s = read(waitFd, &command, sizeof(command));
    if(s == 0)
    {
        quit = true;
        return -1;
    }
    assert(s == sizeof(command));
    return command;
}

void sendAndWakeup(pid_t who, int fd, uint32_t command)
{
    write(fd, &command, sizeof(command));
    cout << "main process: call process " << who << " execute " << desc[command] << " through " << fd << endl;

}

int main()
{
    load();
    //          pid,  pipeid
    vector<pair<pid_t, int>> slots;
    for(int i = 0; i < PROCESS_NUM; i++)
    {
        //1.创建子进程
        int pipefd[2] = {0};
        int n = pipe(pipefd);
        assert(n == 0);
        (void)n;

        pid_t id = fork();
        assert(id != -1);

        //子进程读取
        if(id == 0)
        {
            //child
            //关闭写端
            close(pipefd[1]);
            while(true)
            {
                bool quit = false;
                int command = waitCommand(pipefd[0], quit); //如果对方不发,就阻塞
                if(quit)
                {
                    break;
                }
                //执行命令
                if(command >= 0 && command < handlerSize())
                {
                    callbacks[command]();
                }
                else 
                {
                    cout << "非法command:" << command << endl;
                }
            }
            exit(1);
        }
        //father,关闭读端
        close(pipefd[0]);
        slots.push_back(pair<pid_t, int>(id, pipefd[1]));
    }

    int select;
    int command; 

    //父进程派发任务
    srand((unsigned long)time(nullptr) ^ getpid() ^ 212211212L);
    while(true)
    {
        cout << "################################" << endl;
        cout << "#   1. show funcitons      2.send command  #" << endl;
        cout << "############################################" << endl;
        cout << "Please Select> ";
        cin >> select;
         if (select == 1)     showHandler();
         else if (select == 2)
         {
             cout << "Enter Your Command> ";
             // 选择任务
             cin >> command;
             // 选择进程
             int choice = rand() % slots.size();
             // 把任务给指定的进程
             sendAndWakeup(slots[choice].first, slots[choice].second, command);
         }
         else                                       
         {}
    //关闭id,结束所有进程
    }

    //关闭fd, 所有的子进程都会退出 
    for (const auto &slot : slots)
    {
        close(slot.second);
    }
    
    // 回收所有的子进程信息
    for (const auto &slot : slots)
    {
        waitpid(slot.first, nullptr, 0);
    }            
    return 0;
}


