#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

void handler(int sig)
{
    pid_t id;
    while ((id = waitpid(-1, NULL, WNOHANG)) > 0)
    {
        printf("wait child success: %d\n", id);
    }
    printf("child is quit! %d\n", getpid());
}

int main()
{
    signal(SIGCHLD, handler);
    pid_t cid;
    if ((cid = fork()) == 0)
    { // child
        printf("child : %d\n", getpid());
        sleep(3);
        exit(1);
    }
    while (1)
    {
        printf("father proc is doing some thing!\n");
        sleep(1);
    }
    return 0;
}

// volatile int flag = 0;

// void changeFlag(int sig)
// {
//     (void)sig;
//     cout << "change flag : " << flag;
//     flag = 1;
//     cout << "->" << flag << endl;
// }

// int main()
// {
//     signal(2, changeFlag);

//     while(!flag);
//     cout << "进程正常退出" << flag << endl;
//     return 0;
// }

// void showPending(sigset_t *pending)
// {
//     for (int i = 1; i <= 31; i++)
//     {
//         if (sigismember(pending, i))
//             cout << "1";
//         else
//             cout << "0";
//     }
//     cout << endl;
// }

// void handler(int signum)
// {
//     cout << "获取了一个信号: " << signum << endl;
//     cout << "获取了一个信号: " << signum << endl;
//     cout << "获取了一个信号: " << signum << endl;
//     cout << "获取了一个信号: " << signum << endl;
//     cout << "获取了一个信号: " << signum << endl;
//     cout << "获取了一个信号: " << signum << endl;

//     sigset_t pending;
//     int n = 20;
//     while (true)
//     {
//         sigpending(&pending);
//         showPending(&pending);
//         n--;
//         if (!n)
//             break;
//         sleep(1);
//     }
// }

// int main()
// {
//     // signal(2, SIG_IGN);

//     cout << "getpid : " << getpid() << endl;
//     // 用户栈定义
//     struct sigaction act, oact;
//     act.sa_flags = 0;
//     sigemptyset(&act.sa_mask);

//     act.sa_handler = handler;

//     sigaddset(&act.sa_mask, 3);
//     sigaddset(&act.sa_mask, 4);
//     sigaddset(&act.sa_mask, 5);
//     sigaddset(&act.sa_mask, 6);
//     sigaddset(&act.sa_mask, 7);

//     // 设置进当前调用的进程PCB中
//     sigaction(2, &act, &oact);

//     cout << "default action : " << (int)oact.sa_handler << endl;

//     while (true)
//     {
//         sleep(1);
//         /* code */
//     }

//     return 0;
// }
