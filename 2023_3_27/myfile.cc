#include <iostream>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
using namespace std;

bool SetNonBlock(int fd)
{
    int f1 = fcntl(fd, F_GETFL); // 在底层获取当前fd对应的文件读写标志位
    if (f1 < 0)
        return false;
    fcntl(fd, F_SETFL, f1 | O_NONBLOCK); //设置为非阻塞
    return true;
}
int main()
{
    SetNonBlock(0); // 只要设置一次.后续就都是非阻塞了
    char buffer[1024];
    while (true)
    {
        errno = 0;
        // 出错,不仅仅是错误返回值,errno变量也会被设置,表明出错原因
        ssize_t s = read(0, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s - 1] = 0;
            cout << "echo#" << buffer << " errno[---]: " << errno << " errstring :" << strerror(errno) << endl;
        }
        else
        {
            sleep(1);
            // 如果失败的errno值是11,就代表数据没有就绪
            // cout << "read \"error\"" << " errno : " << errno << " errstring :" << strerror(errno) << endl;
            if(errno == EWOULDBLOCK || errno == EAGAIN)
            {
                cout << "当前0号fd数据没有就绪" << endl;
                continue;
            }
            else if(errno == EINTR)
            {
                cout << "当前IO可能被信号中断了" << endl;
                continue;
            }
            else
            {
                //差错处理
            }
        }
    }
    return 0;
}