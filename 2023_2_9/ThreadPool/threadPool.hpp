#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <unistd.h>

#include "thread.hpp"
#include "LockGuard.hpp"
#include "Log.hpp"

const int g_thread_num = 3;

template <class T>
class ThreadPool
{
public:
    pthread_mutex_t *getMutex()
    {
        return &_lock;
    }
    bool isEmpty()
    {
        return _task_queue.empty();
    }
    void waitCond()
    {
        pthread_cond_wait(&_cond, &_lock);
    }
    T getTask()
    {
        T t = _task_queue.front();
        _task_queue.pop();
        return t;
    }

public:
    ThreadPool(int thread_num = g_thread_num)
        : _num(thread_num)
    {
        for (int i = 1; i <= _num; ++i)
        {
            _threads.push_back(new Thread(i, routine, this));
        }
        pthread_mutex_init(&_lock, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }

    ~ThreadPool()
    {
        for (auto &iter : _threads)
        {
            iter->join();
            delete iter;
        }

        pthread_mutex_destroy(&_lock);
        pthread_cond_destroy(&_cond);
    }

    // 两个参数 类内函数会传this指针
    // 消费过程
    static void *routine(void *args)
    {
        ThreadData *td = (ThreadData *)args;
        ThreadPool<T> *tp = (ThreadPool<T> *)td->_args;
        while (true)
        {
            T task;
            {
                LockGuard lockguard(tp->getMutex());
                while (tp->isEmpty())
                    tp->waitCond();
                // 读取任务
                task = tp->getTask(); // 任务队列是共享的-> 将任务从共享，拿到自己的私有空间
            }
            task(td->_name);
            // lock
            // while(_task_queue.empty()) wait();
            // 获取任务
            // unlock
            // 处理任务
        }
    }


    void run()
    {
        for (auto &iter : _threads)
        {
            iter->start();
            logMessage(NORMAL, "%s %s", iter->name().c_str(), "启动成功");
            // std::cout << iter->name() << " 启动成功" << std::endl;
        }
    }

    // 生产过程
    void pushTask(const T &task)
    {
        LockGuard lockguard(&_lock);
        _task_queue.push(task);
        pthread_cond_signal(&_cond);
    }

private:
    std::vector<Thread *> _threads;
    int _num;
    std::queue<T> _task_queue;
    pthread_mutex_t _lock;
    pthread_cond_t _cond;
};
