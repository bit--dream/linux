#include "BlockQueue.hpp"
#include "Task.hpp"

#include <ctime>

int myAdd(int x, int y)
{
    return x + y;
}

void *consumer(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    while (true)
    {
        Task t;
        bqueue->pop(&t);

        std::cout << pthread_self() << " consumer: " << t._x << "+" << t._y << "=" << t() << std::endl;
        // cout << t._x << " + " << t._y << " = " << t() << endl << endl;
        sleep(1);
    }
    return nullptr;
}

void *productor(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    // int a = 1;
    while (true)
    {
        int x = rand() % 5 + 1;
        usleep(rand() % 1999);
        int y = rand() % 5 + 1;

        // int x, y;
        // cout << "Please Enter x: ";
        // cin >> x;
        // cout << "Please Enter y: ";
        // cin >> y;
        Task t(x, y, myAdd);
        bqueue->push(t);
        std::cout << pthread_self() << " productor: " << t._x << "+" << t._y << "=?" << std::endl;
        // cout << t._x << " + " << t._y << " = ? " << endl;
        sleep(1);
    }
    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid() ^ 0x132);
    BlockQueue<Task> *bqueue = new BlockQueue<Task>();

    pthread_t c[2], p[2];
    pthread_create(c, nullptr, consumer, bqueue);
    pthread_create(c + 1, nullptr, consumer, bqueue);
    pthread_create(p, nullptr, productor, bqueue);
    pthread_create(p + 1, nullptr, productor, bqueue);

    pthread_join(c[0], nullptr);
    pthread_join(c[1], nullptr);
    pthread_join(p[0], nullptr);
    pthread_join(p[1], nullptr);

    delete bqueue;
    return 0;
}
