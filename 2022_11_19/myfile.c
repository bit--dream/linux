#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
    //C语言提供
    printf("hello printf\n");
    fprintf(stdout, "hello fprintf\n");
    const char* s = "hello fputs\n";
    fputs(s, stdout);

    //OS提供
    const char* ss = "hello write\n";
    write(1, ss, strlen(ss));

    fork();//创建子进程



    return 0;
}

//int main(int argc, char* argv[])
//{
//    if(argc != 2)
//    {
//        return 2;
//    }
//    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC);
//    if(fd < 0)
//    {
//        perror("open");
//        return 1;
//    }
//    dup2(fd, 1);
//    fprintf(stdout, "%s\n", argv[1]);
//
//
//
//
//
//
//
//
//
//
//    //close(0);
//    //int fd = open("log.txt", O_RDONLY);
//
//    //if(fd < 0)
//    //{
//    //     perror("open");
//    //     return 1;
//    //}
//    //printf("fd:%d\n", fd);
//    //
//    //char buffer[64];
//    //fgets(buffer, sizeof buffer, stdin);
//
//    //printf("%s\n", buffer);
//
//   // close(1);
//   // // 这里fd的分配规则：最小的，没有被占用的文件描述符
//   // int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//
//   // if(fd < 0)
//   // {
//   //     perror("open");
//   //     return 1;
//   // }
//
//   // printf("fd:%d\n", fd);
//   // printf("fd:%d\n", fd);
//   // printf("fd:%d\n", fd);
//   // printf("fd:%d\n", fd);
//   // printf("fd:%d\n", fd);
//   // fprintf(stdout, "hello fprintf\n");
//   // const char* s = "hello fwrite\n";
//   // fwrite(s, strlen(s), 1, stdout);
//
//   // fflush(stdout);
//   // close(fd);
//   // return 0;
//}
//
