#include "ringQueue.hpp"
#include <cstdlib>
#include <ctime>
#include <sys/types.h>
#include <unistd.h>


void *consumer(void *args)
{
    RingQueue<int> *rq = (RingQueue<int> *)args;
    while (true)
    {
        sleep(1);
        int x;
        // 1. 从环形队列中获取任务或数据
        rq->Pop(&x);

        // 2. 进行处理 -- 不能忽略时间消耗
        std::cout << "消费: " << x << " [" << pthread_self() << "]" << std::endl;
    }
}

void *productor(void *args)
{
    RingQueue<int> *rq = (RingQueue<int> *)args;
    while (true)
    {
        // 1. 构建数据或任务对象 -- 一般是从外部来的 -- 不可以忽略它的时间消耗
        int x = rand() % 100 + 1;
        std::cout << "生产: " << x << " [" << pthread_self() << "]" << std::endl;
        // 2. 推送到环形队列中
        rq->Push(x);
    }
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid());

    RingQueue<int> *rq = new RingQueue<int>();
    pthread_t c[3], p[3];
    pthread_create(c, nullptr, consumer, (void *)rq);
    pthread_create(c + 1, nullptr, consumer, (void *)rq);
    pthread_create(c + 2, nullptr, consumer, (void *)rq);

    pthread_create(p, nullptr, productor, (void *)rq);
    pthread_create(p + 1, nullptr, productor, (void *)rq);
    pthread_create(p + 2, nullptr, productor, (void *)rq);

    for(int i = 0; i < 3; ++i) pthread_join(c[i], nullptr);
    for(int i = 0; i < 3; ++i) pthread_join(p[i], nullptr);
    // rq->Debug();
    return 0;
}
