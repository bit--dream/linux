#ifndef _Ring_QUEUE_HPP_
#define _Ring_QUEUE_HPP_

#include <iostream>
#include <vector>
#include <pthread.h>

#include "sem.hpp"

const int g_default_num = 5;

template <class T>
class RingQueue
{
private:
    std::vector<T> _ringQueue;
    int _num;
    int _c_step; // 消费下标
    int _p_step; // 生产下标
    Sem _space_sem;
    Sem _data_sem;
    pthread_mutex_t clock;
    pthread_mutex_t plock;

public:
    RingQueue(int default_num = g_default_num)
        : _ringQueue(default_num),
          _num(default_num),
          _c_step(0),
          _p_step(0),
          _space_sem(default_num),
          _data_sem(0)
    {
        pthread_mutex_init(&clock, nullptr);
        pthread_mutex_init(&plock, nullptr);
    }
    ~RingQueue()
    {
        pthread_mutex_destroy(&clock);
        pthread_mutex_destroy(&plock);
    }
    // 生产者: 空间资源  临界资源:下标
    void Push(const T &in)
    {
        // 先申请信号量 再加锁
        _space_sem.P();
        pthread_mutex_lock(&plock);
        
        _ringQueue[_p_step++] = in;
        _p_step %= _num;
        
        pthread_mutex_unlock(&plock);
        _data_sem.V();
        
    }
    // 消费者: 数据资源
    void Pop(T *out)
    {
        _data_sem.P();
        pthread_mutex_lock(&clock);
        
        *out = _ringQueue[_c_step++];
        _c_step %= _num;
        
        pthread_mutex_unlock(&clock);
        _space_sem.V();
    }

    // void Debug()
    // {
    //     std::cerr << "size: " << _ringQueue.size() << " num: " << num << std::endl;
    // }
};

#endif
