#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;

namespace std
{
    void test1()
    {
        vector<int> v1;
        vector<int> v2(10, 1);
        vector<int> v3(v2);
    }

    void test2()
    {
        vector<int> v1;
        v1.push_back(1);
        v1.push_back(2);
        v1.push_back(3);
        v1.push_back(4);

        for(size_t i = 0; i < v1.size(); i++)
        {
            v1[i]++;
            cout << v1[i] << " ";
        }
        cout << endl;

        //迭代器
        vector<int>::iterator it = v1.begin();
        while(it != v1.end())
        {
            (*it)--;
            cout << *it << " ";
            it++;
        }
        cout << endl;

        //范围for
        for(auto v : v1)
        {
            cout << v << " ";
        }
        cout << endl;
    }

    void test3()
    {
        vector<int> v1;
        v1.push_back(1);
        v1.push_back(2);
        v1.push_back(3);
        v1.push_back(4);
        vector<int>::iterator pos =  find(v1.begin(), v1.end(), 3);
        if(pos != v1.end())
        {
            v1.insert(pos, 30);
        }

        for(auto v : v1)
        {
            cout << v << " ";
        }
        cout << endl;


        pos =  find(v1.begin(), v1.end(), 3);
        if(pos != v1.end())
        {
            v1.erase(pos);
        }

        for(auto v : v1)
        {
            cout << v << " ";
        }

        cout << endl;
    }

    void test4()
    {
        vector<int> v1;
        v1.push_back(12);
        v1.push_back(2);
        v1.push_back(30);
        v1.push_back(14);
        v1.push_back(1);
        v1.push_back(23);
        v1.push_back(63);
        v1.push_back(4);

        less<int> ls;// <
        greater<int> gt; // >

        sort(v1.begin(), v1.end(), greater<int>());
        //sort(v1.begin(), v1.end(), gt);

        for(auto e : v1)
        {
            cout << e << " ";
        }
        cout << endl;
    }

}


int main()
{
    std::test4();
    //cout << "hello world" << endl;
    return 0;
}





