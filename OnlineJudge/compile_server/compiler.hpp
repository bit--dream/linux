#pragma once

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "../comm/util.hpp"
#include "../comm/log.hpp"

// 只负责代码的编译

namespace my_compiler
{
    using namespace my_util;
    using namespace my_log;

    class Compiler
    {
    public:
        Compiler()
        {
        }
        ~Compiler()
        {
        }

        // 返回值：编译成功：true，否则：false
        // 输入参数：编译的文件名
        // 1234 -> ./temp/1234.cpp
        // 1234 -> ./temp/1234.exe
        // 1234 -> ./temp/1234.stderr
        static bool Compile(const std::string &file_name)
        {
            pid_t res = fork();
            if (res < 0)
            {
                LOG(ERROR) << "[compiler.hpp]"
                           << "内部错误，子进程创建失败"
                           << "\n";
                return false;
            }
            else if (res == 0)
            {
         
                umask(0);
                // 打开错误文件
                int _stderr = open(PathUtil::CompilerError(file_name).c_str(),
                                   O_CREAT | O_WRONLY, 0644);
                if (_stderr < 0)
                {
                    LOG(WARNING) << "[compiler.hpp]"
                                 << "没有成功形成stderr文件"
                                 << "\n";
                    exit(1);
                }
                // 重定向到标准错误到_stderr
                dup2(_stderr, 2);
         
                // 程序替换，并不影响进程的文件描述符表
                // 子进程：调用编译器，完成对代码的编译工作
                execlp("g++", "g++", "-o", PathUtil::Exe(file_name).c_str(),
                       PathUtil::Src(file_name).c_str(), "-std=c++11", "-D COMPILER_ONLINE", nullptr);
                LOG(ERROR) << "[compiler.hpp]"
                           << "启动编译器g++失败，可能是参数错误"
                           << "\n";
                exit(2);
            }
            else
            {
                // 子进程退出
                waitpid(res, nullptr, 0);
        
                // 编译是否成功?看是否生成exe文件
                if (FileUtil::IsFileExists(PathUtil::Exe(file_name)))
                {
                    LOG(INFO) << "[compiler.hpp]" << PathUtil::Src(file_name) << "编译成功"
                              << "\n";
                    return true;
                }
            }
            LOG(ERROR) << "[compiler.hpp]"
                       << "编译失败，没有形成可执行程序"
                       << "\n";
            return false;
        }
    };
}