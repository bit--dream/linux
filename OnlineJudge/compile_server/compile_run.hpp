#pragma once

#include "compiler.hpp"
#include "runner.hpp"
#include "../comm/log.hpp"
#include "../comm/util.hpp"

#include <signal.h>
#include <unistd.h>
#include "jsoncpp/json/json.h"

namespace my_compile_and_run
{
    using namespace my_log;
    using namespace my_util;
    using namespace my_compiler;
    using namespace my_runner;

    class CompileAndRun
    {
    public:
        //code > 0：进程收到了信号导致异常崩溃
        //code < 0：整个过程非运行报错（代码为空，编译报错等）
        //code = 0：整个过程全部完成
        //待完善
        static std::string CodeToDesc(int code, const std::string& file_name)
        {
            std::string desc;
            switch(code)
            {
                case 0:
                    desc = "编译运行成功";
                    break;
                case -1:
                    desc = "提交的代码为空";
                    break;
                case -2:
                    desc = "未知错误";
                    break;
                case -3: 
                    FileUtil::ReadFile(PathUtil::CompilerError(file_name), &desc, true);
                    //std::cout << "compile desc: " << desc << std::endl;
                    break;
                case SIGSEGV: //11
                    desc = "内存超过范围";
                    break;
                case SIGXCPU: //24
                    desc = "cpu使用超时";
                    break;
                case SIGFPE: //8
                    desc = "浮点数溢出";
                    break;
                default:
                    desc = "未知错误: " + std::to_string(code);
                    break;
            }
            return desc;
        }

        //清理临时文件
        static void RemoveTempFile(const std::string &file_name)
        {
            //清理文件的个数是不确定的,知道有哪些文件
            std::string _src = PathUtil::Src(file_name);
            if(FileUtil::IsFileExists(_src)) unlink(_src.c_str());

            std::string _compiler_error = PathUtil::CompilerError(file_name);
            if(FileUtil::IsFileExists(_compiler_error)) unlink(_compiler_error.c_str());

            std::string _execute = PathUtil::Exe(file_name);
            if(FileUtil::IsFileExists(_execute)) unlink(_execute.c_str());

            std::string _stdin = PathUtil::Stdin(file_name);
            if(FileUtil::IsFileExists(_stdin)) unlink(_stdin.c_str());

            std::string _stdout = PathUtil::Stdout(file_name);
            if(FileUtil::IsFileExists(_stdout)) unlink(_stdout.c_str());

            std::string _stderr = PathUtil::Stderr(file_name);
            if(FileUtil::IsFileExists(_stderr)) unlink(_stderr.c_str());
        }

        // 输入：
        // code：用户提交的代码
        // input：用户给自己提交的代码对应的输入（不做处理）
        // cpu_limit：时间要求
        // mem_limit：空间要求

        // 输出：
        // 必填
        // status：状态码
        // reason：请求结果

        // 选填
        // stdout：我的程序运行完的结果
        // stderr：我的程序运行完的错误结果

        // 参数
        // in_json:{"code": "", "input": "", "cpu_limit": 1, "mem_limit": 1024}
        // out_json:{"status": "0", "reason": "", "stdout": "", "stderr": ""}
        static void Start(const std::string &in_json, std::string *out_json)
        {
            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value); // 最后处理差错问题

            // 获取输入
            std::string code = in_value["code"].asString();
            std::string input = in_value["input"].asString();
            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            // 返回输出
            int status_code = 0;
            Json::Value out_value;
            int run_result = 0;
            std::string file_name; // 需要内部形成的唯一文件名

            // 没有数据
            if (code.size() == 0)
            {
                // out_value["status"] = -1; //代码为空
                // out_value["reason"] = "用户提交的代码为空";
                // //序列化过程
                status_code = -1; // 代码为空
                goto END;
            }

            // 形成的文件名只具有唯一性，没有目录没有后缀
            // 毫秒级时间戳+原子性递增唯一性：来保证唯一性
            file_name = FileUtil::UniqFileName();

            // 形成临时src文件
            if (!FileUtil::WriteFile(PathUtil::Src(file_name), code))
            {
                // out_value["status"] = -2; //未知错误
                // out_value["reason"] = "提价的代码发送了未知错误";
                // //序列化过程
                status_code = -2; // 未知错误
                goto END;
            }

            // 编译
            if (!Compiler::Compile(file_name))
            {
                // //编译失败
                // out_value["status"] = -3; //代码编译时候发送了错误
                // out_value["reason"] = FileUtil::ReadFile(PathUtil::CompilerError(file_name));
                // //序列化过程
                status_code = -3; // 编译失败
                goto END;
            }

            run_result = Runner::Run(file_name, cpu_limit, mem_limit);
            // 运行
            if (run_result < 0)
            {
                // out_value["status"] = -2; //未知错误
                // out_value["reason"] = "发送了未知错误";
                // //序列号过程
                status_code = -2; // 未知错误
            }
            else if (run_result > 0)
            {
                // out_value["status"] = run_result; //运行时报错
                // out_value["reason"] = SignalToDesc(run_result); //将信号转化成报错原因
                // //序列化过程
                status_code = run_result; // 程序运行时崩溃
            }
            else
            {
                // out_value["status"] = 0; //编译并且运行成功
                // out_value["reason"] = "编译并且运行成功";
                status_code = 0; // 成功
            }
        END:
            out_value["status"] = status_code;

            //test
            // std::cout << "compile_run.hpp file_name:" << file_name << std::endl;
            out_value["reason"] = CodeToDesc(status_code, file_name);
            if (status_code == 0)
            {
                // 整个过程全部成功
                std::string _stdout;
                FileUtil::ReadFile(PathUtil::Stdout(file_name), &_stdout, true);
                out_value["stdout"] = _stdout;

                std::string _stderr;
                FileUtil::ReadFile(PathUtil::Stderr(file_name), &_stderr, true);
                out_value["stderr"] = _stderr;
            }

            // 序列号
            Json::StyledWriter writer;

            //test
            // std::cout << "compile_run.hpp out_value: " << out_value << std::endl;
            *out_json = writer.write(out_value);

            RemoveTempFile(file_name);
        }
    };
}
