#include <iostream>
#include <string>
#include <thread>
#include <unistd.h>
#include <cstring>
#include <pthread.h>
#include <time.h>
#include <assert.h>

using namespace std;
#define THREAD_NUM 5

class ThreadData
{
public:
    ThreadData(const string &tname, pthread_mutex_t *pmtx)
        : _tname(tname)
        , _pmtx(pmtx)
    {}

public:
    string _tname;
    pthread_mutex_t *_pmtx;
};

// 加锁保护
// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER; // 全局或static
int tickets = 10000; // 临界资源

void *getTickets(void *args)
{
    ThreadData * td = (ThreadData*)args;
    while (true)
    {
        // 加锁
        int n = pthread_mutex_lock(td->_pmtx);
        assert(n == 0);

        // cout << "申请锁成功" << endl; 
        // 临界区

        if (tickets > 0)
        {
            usleep(rand() % 1456);
            printf("%s: %d \n", td->_tname.c_str(), tickets);
            tickets--;
            // 解锁
            n = pthread_mutex_unlock(td->_pmtx);
            assert(n == 0);
        }
        else
        {
            // 解锁
            n = pthread_mutex_unlock(td->_pmtx);
            assert(n == 0);
            break;
        }
        usleep(rand() % 1234);
    }
    delete td;
    return nullptr;
}

int main()
{
    time_t start = time(nullptr);
    pthread_mutex_t mtx;
    pthread_mutex_init(&mtx, nullptr);

    srand((unsigned long)time(nullptr) ^ getpid() ^ 0x134);

    pthread_t t[THREAD_NUM];

    for (int i = 0; i < THREAD_NUM; i++)
    {
        string name = "thread ";
        name += to_string(i + 1);

        ThreadData *td = new ThreadData(name, &mtx);
        pthread_create(t + i, nullptr, getTickets, (void*)td);
    }

    for(int i = 0; i < THREAD_NUM; i++)
    {
        pthread_join(t[i], nullptr);
    }

    // pthread_create(&t1, nullptr, getTickets, (void *)"thread one");
    // pthread_create(&t2, nullptr, getTickets, (void *)"thread two");
    // pthread_create(&t3, nullptr, getTickets, (void *)"thread three");

    // pthread_join(t1, nullptr);
    // pthread_join(t2, nullptr);
    // pthread_join(t3, nullptr);

    pthread_mutex_destroy(&mtx);

    time_t end = time(nullptr);

    cout << "cast : " << (int)(end - start) << "S" << endl;
    return 0;
}
