#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <cstdio>

typedef void *(*fun_t)(void *);

class ThreadData
{
public:
    void *_args;
    std::string _name;
};

class Thread
{
public:
    Thread(int num, fun_t callback, void *args)
        : _func(callback)
    {
        char nameBuffer[64];
        snprintf(nameBuffer, sizeof nameBuffer, "Thraed-%d", num);
        _name = nameBuffer;

        _tdata._args = args;
        _tdata._name = _name;
    }
    ~Thread()
    {
    }

    void start()
    {
        pthread_create(&_tid, nullptr, _func, (void*)&_tdata);
    }

    void join()
    {
        pthread_join(_tid, nullptr);
    }

    std::string name()
    {
        return _name;
    }

private:
    std::string _name;
    pthread_t _tid;
    fun_t _func;
    ThreadData _tdata;
};
