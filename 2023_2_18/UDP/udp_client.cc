#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <memory>
#include "thread.hpp"

#define SIZE 1024

uint16_t server_port = 0;
std::string server_ip;

static void usage(std::string proc)
{
    std::cout << "\nUsage: " << proc << "client_ip client_port\n"
              << std::endl;
}

static void *udpSend(void *args)
{
    int sock = *(int *)((ThreadData *)args)->_args;
    std::string name = ((ThreadData *)args)->_name;

    std::string message;
    struct sockaddr_in server;
    memset(&server, 0, sizeof server);
    server.sin_family = AF_INET;
    server.sin_port = htons(server_port);
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());

    while (true)
    {
        std::cerr << "请输入你的信息# ";
        std::getline(std::cin, message);
        if (message == "quit")
            break;
        // 当client首次发送消息给服务器时,OS会自动给client bind一个IP和port
        sendto(sock, message.c_str(), sizeof message, 0, (struct sockaddr *)&server, sizeof server);
    }

    return nullptr;
}

static void *udpRecv(void *args)
{
    int sock = *(int *)((ThreadData *)args)->_args;
    std::string name = ((ThreadData *)args)->_name;

    char buffer[SIZE];
    while (true)
    {
        memset(buffer, 0, sizeof buffer);
        struct sockaddr_in temp;
        socklen_t len = sizeof temp;
        ssize_t s = recvfrom(sock, buffer, sizeof buffer, 0, (struct sockaddr *)&temp, &len);
        if (s > 0)
        {
            buffer[s] = 0;
            std::cout << buffer << std::endl;
        }
    }
    return nullptr;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage(argv[0]);
        exit(1);
    }
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        std::cerr << "socket error " << std::endl;
        exit(2);
    }

    server_ip = argv[1];
    server_port = atoi(argv[2]);
    std::unique_ptr<Thread> sender(new Thread(1, udpSend, (void *)&sock));
    std::unique_ptr<Thread> recver(new Thread(2, udpRecv, (void *)&sock));

    sender->start();
    recver->start();

    sender->join();
    recver->join();
    // client 一般不会显示bind指定port,而是让OS自动随机选择
    // std::string message;
    // struct sockaddr_in server;
    // memset(&server, 0, sizeof server);
    // server.sin_family = AF_INET;
    // server.sin_port = htons(atoi(argv[2]));
    // server.sin_addr.s_addr = inet_addr(argv[1]);

    // char buffer[SIZE];
    // while (true)
    // {
    //     std::cout << "请输入你的信息# ";
    //     std::getline(std::cin, message);
    //     if(message == "quit") break;
    //     // 当client首次发送消息给服务器时,OS会自动给client bind一个IP和port
    //     sendto(sock, message.c_str(), sizeof message, 0, (struct sockaddr*) &server, sizeof server);

    //     struct sockaddr_in temp;
    //     socklen_t len = sizeof temp;
    //     ssize_t s = recvfrom(sock, buffer, sizeof buffer, 0, (struct sockaddr*)&temp, &len);
    //     if(s > 0)
    //     {
    //         buffer[s] = 0;
    //         std::cout << "server echo# " << buffer << std::endl;
    //     }
    // }
    close(sock);
    return 0;
}