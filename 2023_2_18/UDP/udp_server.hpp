#ifndef _UDP_SERVER_HPP
#define _UDP_SERVER_HPP

#include "Log.hpp"
#include <iostream>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <queue>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define SIZE 1024

class UdpServer
{
private:
    std::string _ip;
    uint16_t _port;
    int _socket;
    std::unordered_map<std::string, struct sockaddr_in> _users;
    std::queue<std::string> _messageQueue;
    
public:
    UdpServer(uint16_t port, std::string ip = "")
        : _port(port), _ip(ip), _socket(-1)
    {
    }

    bool initServer()
    {
        // 1.创建套接字
        _socket = socket(AF_INET, SOCK_DGRAM, 0);
        if (_socket < 0)
        {
            logMessage(FATAL, "%d:%d", errno, strerror(errno));
            exit(2);
        }
        // 2. bind:将用户设置的ip和port在内核中和当前进程强关联
        struct sockaddr_in local;
        bzero(&local, sizeof local);
        local.sin_family = AF_INET;

        // 先将数据发送到网络
        local.sin_port = htons(_port); // 主机转网络

        // 1. 先将点分十进制字符串IP -> 4字节
        // 2. 4字节主机序列 -> 网络序列
        // 让服务器在工作工程中,可以从任意IP中获取数据
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());

        if (bind(_socket, (struct sockaddr *)&local, sizeof local) < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        logMessage(NORMAL, "init udp sever done ... %s", strerror(errno));
        return true;
    }

    void Start()
    {
        char buffer[SIZE];
        char key[64];
        while (1)
        {
            struct sockaddr_in peer;
            bzero(&peer, sizeof(peer));
            char result[256];
            std::string cmd_echo;
            // 输入: peer 缓冲区大小
            // 输出: 实际读到的peer
            socklen_t len = sizeof(peer);
            ssize_t ret = recvfrom(_socket, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
            if (ret > 0)
            {
                buffer[ret] = 0;
                // 1.输出发送的数据信息
                // 2.是谁发送的信息
                // 发过来的是指令
                // if (strcasestr(buffer, "rm") != nullptr || strcasestr(buffer, "rmdir") != nullptr)
                // {
                //     std::string err_message = "坏小子...";
                //     std::cout << err_message << buffer << std::endl;
                //     sendto(_socket, err_message.c_str(), err_message.size(), 0, (struct sockaddr *)&peer, len);

                //     continue;
                // }

                // FILE *fp = popen(buffer, "r");
                // if (nullptr == fp)
                // {
                //     logMessage(ERROR, "popen :%d:%s", errno, strerror(errno));
                //     continue;
                // }

                // while (fgets(result, sizeof result, fp) != nullptr)
                // {
                //     cmd_echo += result;
                // }

                // fclose(fp);
                uint16_t cli_port = ntohs(peer.sin_port);
                std::string cli_ip = inet_ntoa(peer.sin_addr);
                // printf("[%s:%d]# %s\n", cli_ip.c_str(), cli_port, buffer);

                snprintf(key, sizeof key, "%s-%d", cli_ip.c_str(), cli_port);
                logMessage(NORMAL, "key:%s", key);
                auto it = _users.find(key);
                if (it == _users.end())
                {
                    logMessage(NORMAL, "add new user : %s", key);
                    _users.insert({key, peer});
                }
            }

            // 分析和处理数据
            // 写回数据
            // sendto(_socket, buffer, strlen(buffer), 0, (struct sockaddr *)&peer, len);
            // sendto(_socket, cmd_echo.c_str(), cmd_echo.size(), 0, (struct sockaddr *)&peer, len);

            for (auto &iter : _users)
            {
                std::string sendMessage = key;
                sendMessage += "#";
                sendMessage += buffer;
                logMessage(NORMAL, "push massage to %s", iter.first.c_str());
                sendto(_socket, sendMessage.c_str(), sendMessage.size(), 0, (struct sockaddr *)&iter.second, sizeof iter.second);
            }
        }
    }
    ~UdpServer()
    {
        if (_socket >= 0)
            close(_socket);
    }
};

#endif