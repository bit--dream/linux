#include <iostream>
#include <cerrno>
#include <cstring>
#include <ctime>
#include <cassert>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

using namespace std;

bool SetNonBlock(int fd)
{
    int f1 = fcntl(fd, F_GETFL); // 在底层获取当前fd对应的文件读写标志位
    if (f1 < 0)
        return false;
    fcntl(fd, F_SETFL, f1 | O_NONBLOCK); // 设置为非阻塞
    return true;
}

int main()
{
    while (true)
    {
        cout << "time: " << (unsigned long)time(nullptr) << endl;
        struct timeval currtime = {0, 0};
        int n = gettimeofday(&currtime, nullptr);
        assert(n == 0);
        (void)n;
        cout << "gettimeofday : " << currtime.tv_sec << " . " << currtime.tv_usec << endl;
    }
    return 0;
}