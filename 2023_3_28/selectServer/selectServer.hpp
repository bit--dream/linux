#ifndef _SELECT_SVR_H_
#define _SELECT_SVR_H_

#include <iostream>
#include <string>
#include <sys/select.h>
#include <sys/time.h>
#include "Log.hpp"
#include "Sock.hpp"

#define BITS 8
#define NUM (sizeof(fd_set) * BITS)
#define FD_NONE -1

using namespace std;

class SelectServer
{
public:
    SelectServer(const uint16_t &port = 8080)
        : _port(port)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        logMessage(DEBUG, "create base socket success");
        for (int i = 0; i < NUM; i++)
            _fd_arr[i] = FD_NONE;

        _fd_arr[0] = _listensock;
    }
    void Start()
    {
        while (true)
        {
            // struct timeval timeout = {0, 0};
            // listensock 可以看成是IO,input事件,如果没有连接到来,就阻塞了
            // int sock = Sock::Accept(_listensock, ...); // 不能直接调用accept了
            // FD_SET(_listensock, &rfds); // 将listensoock添加到读文件描述符集中
            // int n = select(_listensock + 1, &rfds, nullptr, nullptr, nullptr);
            // nfds ：随着我们获取的sock越来越多,注定了nfds每一次都可能要变化,我们需要对它动态计算
            // rfds/writefds/exceptfds:都是输入输出型参数,输入输出不一定是一样的,所以我们每次要对rfds进行重新添加
            // timeout每一次也要进行重置
            // 要将合法的文件描述符全部保存起来 来支持:1.更新最大fd 2.更新位图结构

            DebugPrint();
            fd_set rfds;

            FD_ZERO(&rfds);
            int maxfd = _listensock;
            for (int i = 0; i < NUM; i++)
            {
                if (_fd_arr[i] == FD_NONE)
                    continue;
                FD_SET(_fd_arr[i], &rfds);
                if (maxfd < _fd_arr[i])
                    maxfd = _fd_arr[i];
            }

            int n = select(maxfd + 1, &rfds, nullptr, nullptr, nullptr);
            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out ...");
                break;
            case -1:
                logMessage(WARNING, "select error ... %d : %s", errno, strerror(errno));
            default:
                logMessage(DEBUG, "get a new link event ...");
                HandlerEvent(rfds);
                break;
            }
        }
    }

    ~SelectServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    void Accepter()
    {
        string clientip;
        uint16_t clientport = 0;
        // listensock上面的读事件就绪了,表示可以读取了
        // 获取新连接
        int sock = Sock::Accept(_listensock, &clientport, &clientip); // accept 不会阻塞
        if (sock < 0)
        {
            logMessage(WARNING, "accept error ...");
            return;
        }
        logMessage(DEBUG, "grt a new line success : [%s : %d], %d ", clientip.c_str(), clientport, sock);

        // 不能直接read/recv 我们不清楚该sock上面的数据什么时候到来 read就会阻塞,只有select清楚数据有没有准备好
        // 得到新链接时,此时我们应该考虑的是,将新的sock托管给select,让select帮我们检测sock上是否有新的数据
        // 读事件就绪,select就会通知我,我们再进行读取,此时我们就不会被阻塞了
        // 要将sock添加给select, 其实我们只要将fd放入到数组中即可
        int pos = 1;
        for (; pos < NUM; pos++)
        {
            if (_fd_arr[pos] == FD_NONE)
                break;
        }
        if (pos == NUM)
        {
            logMessage(WARNING, "select server already full, close :%d", sock);
            close(sock);
        }
        else
        {
            _fd_arr[pos] = sock;
        }
    }
    void Recver(int i)
    {
        // 读事件就绪:INPUT事件到来, recv\read
        logMessage(DEBUG, "message in, get IO event : %d", _fd_arr[i]);
        // 这里的读取是有问题的,你怎么保证一次读到一个完整报文呢
        char buffer[1024];
        int n = recv(_fd_arr[i], buffer, sizeof(buffer - 1), 0);
        if (n > 0)
        {
            buffer[n] == 0;
            logMessage(DEBUG, "client[%d]# %s", _fd_arr[i], buffer);
        }
        else if (n == 0) // 对方把链接关了
        {
            logMessage(DEBUG, "client[%d] quit, me too ...", _fd_arr[i]);
            // 1.我们也要关闭不需要的fd
            close(_fd_arr[i]);
            // 2.不需要让select帮我关心当前的fd了
            _fd_arr[i] = FD_NONE;
        }
        else
        {
            logMessage(WARNING, "%d sock recv error, %d : %s", _fd_arr[i], errno, strerror(errno));
            // 1.我们也要关闭不需要的fd
            close(_fd_arr[i]);
            // 2.不需要让select帮我关心当前的fd了
            _fd_arr[i] = FD_NONE;
        }
    }
    void HandlerEvent(const fd_set &rfds) // 是一个集合,里面可能会存在多个sock
    {
        for (int i = 0; i < NUM; i++)
        {
            // 1.去掉不合法的fd
            if (_fd_arr[i] == FD_NONE)
                continue;
            // 2.合法的也不一定就绪
            if (FD_ISSET(_fd_arr[i], &rfds))
            {
                // 指定fd读事件就绪
                if (i == 0 && _fd_arr[i] == _listensock)
                {
                    // 读事件就绪:链接事件到来
                    Accepter();
                }
                else
                {
                    Recver(i);
                }
            }
        }
    }

    void DebugPrint()
    {
        cout << "_fd__array[] : ";
        for (int i = 0; i < NUM; i++)
        {
            if (_fd_arr[i] == FD_NONE)
                continue;
            cout << _fd_arr[i] << " ";
        }
        cout << endl;
    }

private:
    uint16_t _port;
    int _listensock;
    int _fd_arr[NUM];
};

#endif