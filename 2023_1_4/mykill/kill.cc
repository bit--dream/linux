#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

typedef function<void ()> func;
vector<func> callbacks;

uint64_t count = 0; 

void showCount()
{
    cout << "final count : " << count << endl;
}

void showLog()
{
    cout << "这个是日志" << endl;
}

void logUser()
{
    if(fork() == 0)
    {
        execl("/usr/bin/who", "who", nullptr);
        exit(1);
    }
    wait(nullptr);
}



void catchSignal(int signum)
{
    for(auto &f : callbacks)
    {
        f();
    }
    alarm(1);
}

static void Usage(string proc)
{
    cout << "Usage:\r\n\t" << proc << " signum processid " << endl;
}

int main(int argc, char *argv[])
{
    alarm(1);

    signal(SIGALRM, catchSignal);

    callbacks.push_back(showCount);
    callbacks.push_back(showLog);
    callbacks.push_back(logUser);

    while (true)
    {
        count++;
    }

    // cout << "开始运行" << endl;
    // abort();//可以终止进程
    // //raise(6);
    // //kill(getpid(), 6);
    // if(argc != 3)
    // {
    //     Usage(argv[0]);
    //     exit(1);
    // }

    // int signum = atoi(argv[1]);

    // int procid = atoi(argv[2]);

    // kill(procid, signum);
    return 0;
}