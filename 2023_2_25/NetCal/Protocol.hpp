#pragma once
#include <iostream>
#include <cstring>
#include <string>

#define SIZE 1024

namespace ns_protocol
{

#define MYSELF 1
#define SPACE " "
#define SPACE_LEN strlen(SPACE)
    class Request
    {
    public:
        // "_x _op _y"
        std::string Serialize() // 序列化
        {
#ifdef MYSELF
            std::string str;
            str = std::to_string(_x);
            str += SPACE;
            str += _op;
            str += SPACE;
            str += std::to_string(_y);
            return str;
#else
            std::cout << "TODD" << std::endl;
#endif
        }

        bool Deserialize(const std::string &package) //  反序列化
        {
#ifdef MYSELF
            std::size_t left = package.find(SPACE);
            if (left == std::string::npos)
                return false;
            std::size_t right = package.rfind(SPACE);
            if (left == std::string::npos)
                return false;

            _x = atoi(package.substr(0, left).c_str());
            if (left + SPACE_LEN > package.size())
                return false;
            else
                _op = package[left + SPACE_LEN];
            _y = atoi(package.substr(right + SPACE_LEN).c_str());

            return true;

#else
            std::cout << "TODD" << std::endl;
#endif
        }

    public:
        Request()
        {
        }

        Request(int x, int y, char op) : _x(x), _y(y), _op(op)
        {
        }

        ~Request() {}

    public:
        int _x;
        int _y;
        char _op;
    };

    class Response
    {
    public:
        std::string Serialize()
        {
#ifdef MYSELF
            std::string s;
            s = std::to_string(_code);
            s += SPACE;
            s += std::to_string(_result);

            return s;

#else
            std::cout << "TODD" << std::endl;
#endif
        }
        bool Deserialize(const std::string &package)
        {
#ifdef MYSELF
            std::size_t pos = package.find(SPACE);
            if (pos == std::string::npos)
                return false;
            
            _code = atoi(package.substr(0, pos).c_str());
            _result = atoi(package.substr(pos + SPACE_LEN).c_str());

            return true;

#else
            std::cout << "TODD" << std::endl;
#endif
        }

    public:
        Response()
        {
        }
        Response(int result, int code) : _result(result), _code(code)
        {
        }
        ~Response() {}

    public:
        int _result; // 计算结果
        int _code;   // 计算结果的状态码
    };

    static std::string Recv(int sock)
    {
        char inbuffer[1024];
        ssize_t s = recv(sock, inbuffer, sizeof(inbuffer), 0);
        if (s > 0)
            return inbuffer;
    }

    static void Send(int sock, const std::string str)
    {
        send(sock, str.c_str(), str.size(), 0);
    }
}