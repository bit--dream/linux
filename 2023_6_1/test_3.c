#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main()
{
	pid_t id = fork();
	if(id == 0){ //child
		int count = 5;
		while(count){
			printf("I am child process PID : %d, PPID : %d, count : %d\n", getpid(), getppid(), count);
			sleep(1);
			count --;
		}
		printf("child quit !\n");
		exit(1);
	}
	else if(id > 0){ //father
		while(1){
			printf("I am father process PID : %d, PPID : %d\n", getpid(), getppid());
			sleep(1);
		}
	}
	else{ 
        printf("create child process failed!\n");
	}
	return 0;
} 
