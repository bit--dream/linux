#include <stdio.h>
#include <unistd.h>

int main()
{
    pid_t id = fork();
    if(id == 0) // child
    {
        while(1)
        {
            printf("I am child process  PID : %d\n", getpid());
            sleep(1);
        }
    }
    else if(id > 0) //parent
    {
        while(1)
        {
            printf("I am parent process PID : %d\n", getpid());
            sleep(1);
        }
    }
    else {
        printf("create child process failed!");
    }
    return 0;
}