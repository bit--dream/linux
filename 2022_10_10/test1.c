#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    int n = 0;
    cin >> n;
    vector<long long> vt;
    int tmp = 3 * n;
    long long a_i = 0;
    long long sum = 0;
    while(tmp--)
    {
        cin >> a_i;
        vt.push_back(a_i);
    }
    sort(vt.begin(), vt.end());
    for(int i = 3 * n - 2; i >= n; i -= 2)
    {
        sum += vt[i];                                           
    }
    cout << sum << endl;
    return 0;

}
