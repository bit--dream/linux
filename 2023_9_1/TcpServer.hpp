#pragma once

#include <iostream>
#include <functional>
#include <string>
#include <cerrno>
#include <unordered_map>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"

class TcpServer;
class Connection;
using func_t = std::function<void(Connection *)>;

// 为了能够正常工作,常规的sock必须要有自己独立的接收缓冲区&&发送缓冲区
class Connection
{

public:
    Connection(int sock = -1)
        : _sock(sock), _tsvr(nullptr)
    {
    }

    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    int _sock; // 负责进行IO的文件描述符

    func_t _recv_cb;   // 读回调
    func_t _send_cb;   // 写回调
    func_t _except_cb; // 异常回调

    std::string _inbuffer; // 暂时没有办法处理二进制流,文本是可以的
    std::string _outbuffer;

    TcpServer *_tsvr; // 对TcpServer的回值指针
};

class TcpServer
{
    const static uint16_t gport = 8080;
    const static uint16_t gnum = 128;

public:
    TcpServer(uint16_t port = gport)
        : _port(port), _revs_num(gnum)
    {
        // 1. 创建listensock
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 2. 创建epoll对象
        _poll.CreateEpoll();

        // 3. 添加listensock到服务器中
        AddConnection(_listensock, std::bind(&TcpServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);

        // 4.构建一个获取就绪事件的缓冲区
        _revs = new struct epoll_event[_revs_num];
    }
    // 专门针对任意sock进行添加TcpServer
    void AddConnection(int sock, func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        Sock::SetNonBlock(sock);
        // 1.构建Connection对象, 封装sock
        Connection *conn = new Connection(_listensock);
        conn->SetCallBack(recv_cb, send_cb, except_cb);
        conn->_tsvr = this;

        // 2. 添加sock到epoll模型
        _poll.AddSockToEpoll(sock, EPOLLIN | EPOLLET); // 任何多路转接的服务器,一般默认只会打开对读取事件的关心,写入事件按需打开

        // 3.将对应的Connection*对象指针添加到Connections集合中
        _connections.insert(std::make_pair(sock, conn));
    }

    void Accepter(Connection *conn)
    {
        // logMessage(DEBUG, "Accepter been called");
        // 一定有listensock已经就绪,此次读取不会阻塞
        // 要保证底层的链接全部都被取走了
        while (true)
        {
            std::string clientip;
            uint16_t clientport;
            int accept_errno = 0;
            int sock = Sock::Accept(conn->_sock, &clientport, &clientip, &accept_errno);
            if (sock < 0)
            {
                if (accept_errno == EAGAIN || accept_errno == EWOULDBLOCK)
                    break;
                else if (accept_errno == EINTR) // 信号中断
                    continue;
                else
                {
                    // accept失败
                    logMessage(WARNING, "accept error, %d : %s", accept_errno, strerror(accept_errno));
                    break;
                }
            }
            if (sock >= 0)
            {
                // 将sock托管给TcpServer
                AddConnection(sock, std::bind(&TcpServer::Recver, this, std::placeholders::_1),
                              std::bind(&TcpServer::Sender, this, std::placeholders::_1),
                              std::bind(&TcpServer::Excepter, this, std::placeholders::_1));

                logMessage(DEBUG, "accept client %s:%d success, add to epoll && TcpServer success, sock:%d",
                           clientip.c_str(), clientport, sock);
            }
        }
    }

    void Recver(Connection *conn)
    {
        const int num = 1024;
        // logMessage(DEBUG, "Recver event exists, Recver() been called");
        // 面向字节流,进行常规读取
        while (true)
        {
            char buffer[num];
            int n = recv(conn->_sock, buffer, sizeof(buffer) - 1, 0);
            if(n < 0)
            {
                if(errno == EAGAIN || errno == EWOULDBLOCK)
                {
                    break;
                }
                else if(errno == EINTR) continue;
                else
                {
                    logMessage(ERROR, "recv error, %%d : %s", errno, strerror(errno));
                    conn->_except_cb(conn);
                    break;
                }
            }
        }  
    }

    void Sender(Connection *conn)
    {
    }

    void Excepter(Connection *conn)
    {
    }

    bool IsConnectionExists(int sock)
    {
        auto iter = _connections.find(sock);
        if (iter == _connections.end())
            return false;
        return true;
    }
    void LoopOnce()
    {
        int n = _poll.WaitEpoll(_revs, _revs_num);
        for (int i = 0; i < n; i++)
        {
            int sock = _revs[i].data.fd;
            uint32_t revents = _revs[i].events;
            if (revents & EPOLLIN)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_recv_cb)
                {
                    _connections[sock]->_recv_cb(_connections[sock]);
                }
            }
            if (revents & EPOLLOUT)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_send_cb)
                {
                    _connections[sock]->_send_cb(_connections[sock]);
                }
            }
        }
    }

    // 根据就绪事件,进行特定事件的派发
    void Dispatcher()
    {
        while (true)
        {
            LoopOnce();
        }
    }

    ~TcpServer()
    {
        if (_listensock >= 0)
            close(_listensock);
        if (_revs)
            delete[] _revs;
    }

private:
    int _listensock;
    uint16_t _port;
    Epoll _poll;
    // sock : connection
    std::unordered_map<int, Connection *> _connections;
    struct epoll_event *_revs;
    int _revs_num;
};
