#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <mutex>
#include <cassert>
#include "oj_model.hpp"
#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "oj_view.hpp"

namespace my_control
{
    // 提供服务的主机
    class Machine
    {
    public:
        std::string _ip;  // 编译服务的ip
        int _port;        // 编译服务的port
        uint64_t _load;   // 编译服务的负载
        std::mutex *_mtx; // mutex禁止拷贝,使用指针
    public:
        Machine() : _ip(""), _port(0), _load(0), _mtx(nullptr)
        {

        }
        ~Machine()
        {
        }
    };

    const std::string service_machine = "./conf/service_machine.conf";
    // 负载均衡
    class LoadBlance
    {
    private:
        // 可以给我们提供编译服务的所有主机
        // 每一台主机都有自己的下标,充当当前主机的id
        std::vector<Machine> _machines; 
        // 所有在线的主机id
        std::vector<int> _online;
        // 所有离线的主机id
        std::vector<int> _offline;
    public:
        LoadBlance()
        {
            assert(LoadConf(service_machine));
        }
        ~LoadBlance()
        {}
    public:
        // 加载配置文件
        bool LoadConf(const std::string &machine_conf)
        {
            std::ifstream in(machine_conf);
            if(!in.is_open())
            {
                my_log::LOG(FATAL) << " 加载: " << machine_conf << " 失败 " << std::endl;
                return false;
            }
            in.close();
            return true; 
        }

        bool SmartChoice()
        {

        }

        void OfflineMachine()
        {

        }

        void OnlineMachine()
        {

        }
    };

    // 核心业务逻辑的控制器
    class Control
    {
    private:
        my_model::Model _model; // 提供后台数据
        my_view::View _view;    // 提供html渲染功能

    public:
        Control()
        {
        }
        ~Control()
        {
        }

        // 根据题目数据构建网页
        // html 输出型参数
        bool AllQuestions(std::string *html)
        {
            bool ret = true;
            std::vector<my_model::Question> all;
            if (_model.GetAllquestions(&all))
            {
                // 获取题目信息成功,将所有的题目数据构建成网页
                _view.AllExpandHtml(all, html);
            }
            else
            {
                *html = "获取题目列表失败,形成题目列表失败";
                ret = false;
            }
            return ret;
        }

        bool Question(std::string number, std::string *html)
        {
            bool ret = true;
            my_model::Question q;
            if (_model.GetOneQuestions(number, &q))
            {
                // 获取指定题目信息成功,将题目数据构建成网页
                _view.OneExpandHtml(q, html);
            }
            else
            {
                *html = "指定题目: " + number + " 不存在!";
                ret = false;
            }
            return ret;
        }
        // id : 100
        // code : #include
        // input : ""
        void Judge(const std::string in_json, std::string *out_json)
        {
            // 1. in_json 进行反序列化,得到题目的id, 得到用户提交的源代码,input

            // 2. 拼接用户代码 + 测试用例代码,形成新的代码

            // 3. 选择负载最低的主机(差错处理)

            // 4. 然后发送http请求,得到结果

            // 5. *out_json = 将结果赋值给out_json
        }
    };
}