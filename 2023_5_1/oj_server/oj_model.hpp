#pragma once
// 根据题目list文件, 加载所有题目信息到内存中
// model: 主要用来和数据进行交互,对外提供访问数据的接口
#include "../comm/log.hpp"
#include "../comm/util.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <cassert>
#include <cstdlib>

namespace my_model
{
    struct Question
    {
        std::string number; // 题目编号,唯一
        std::string title;  // 题目的标题
        std::string star;   // 难度: 简单 中等 困难
        int cpu_limit;      // 题目的时间要求
        int mem_limit;      // 题目的空间要求
        std::string decs;   // 题目的描述
        std::string header; // 题目预设给用户在线编辑器的代码
        std::string tail;   // 题目的测试用例,需要和header拼接,形成完整代码
    };
    const std::string questions_list = "./questions/questions.list";
    const std::string questions_path = "./questions/";
    class Model
    {
    private:
        // 题号 : 题目细节
        std::unordered_map<std::string, Question> questions;

    public:
        Model()
        {
            assert(LoadQuestionList(questions_list));
        }
        bool LoadQuestionList(const std::string &questions_list)
        {
            // 加载配置文件: questions/questions.list + 题目编号文件
            std::ifstream in(questions_list);
            if (!in.is_open())
            {
                my_log::LOG(FATAL) << " 加载题库失败, 请检查是否存在题库文件" << std::endl;
                return false;
            }

            std::string line;
            while (getline(in, line))
            {
                std::vector<std::string> tokens;
                my_util::StringUtil::SplitString(line, &tokens, " ");

                // 1 判断回文数 简单 1 30000
                if (tokens.size() != 5)
                {
                    my_log::LOG(WARNING) << "加载部分题目失败,请检查文件格式" << std::endl;
                    continue;
                }
                
                Question q;
                q.number = tokens[0];
                q.title = tokens[1];
                q.star = tokens[2];
                q.cpu_limit = atoi(tokens[3].c_str());
                q.mem_limit = atoi(tokens[4].c_str());

                std::string path = questions_path;
                path += q.number;
                path += "/";
                my_util::FileUtil::ReadFile(path + "desc.txt", &(q.decs), true);
                my_util::FileUtil::ReadFile(path + "header.cpp", &(q.header), true);
                my_util::FileUtil::ReadFile(path + "tail.cpp", &(q.tail), true);

                questions.insert({q.number, q});

            }
            my_log::LOG(INFO) << "加载题库...成功!" << std::endl;
            in.close();
            return true;
        }
        bool GetAllquestions(std::vector<Question> *out)
        {
            if (questions.size() == 0)
            {
                my_log::LOG(ERROR) << "用户获取题库失败!" << std::endl;
                return false;
            }
            for (const auto &q : questions)
            {
                out->push_back(q.second);
            }
            return true;
        }

        bool GetOneQuestions(const std::string &number, Question *q)
        {
            const auto &iter = questions.find(number);
            if (iter == questions.end())
            {
                my_log::LOG(ERROR) << "用户获取题目失败!,题目编号:" << number << std::endl;
                return false;
            }
            *q = iter->second;
            return true;
        }
        ~Model()
        {
        }
    };
}
