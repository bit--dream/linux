#include <iostream>
#include <vector>
#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <signal.h>
#include <jsoncpp/json/json.h>
#include <boost/algorithm/string.hpp>
#include <ctemplate/template.h>

int main()
{
    std::string in_html = "./test.html";
    std::string value = "hello world";
    
    // 形成数据字典
    ctemplate::TemplateDictionary root("test"); // unordered_map<> test;
    root.SetValue("key", value);                // test.insert({})

    // 获取被渲染网页对象
    ctemplate::Template *tpl = ctemplate::Template::GetTemplate(in_html,ctemplate::DO_NOT_STRIP);

    // 添加字典数据到网页
    std::string out_html;
    tpl->Expand(&out_html, &root);

    // 完成了渲染
    std::cout << out_html << std::endl;
    return 0;
}

// int main()
// {
//     std::vector<std::string> tokens;
//     const std::string str = "1   判断回文数 简单 1 30000";
//     std::string sep = " ";
//     // 输出的字符串 原字符串 分隔符 是否进行压缩 
//     boost::split(tokens, str,boost::is_any_of(sep), boost::algorithm::token_compress_on);

//     for(auto &iter : tokens)
//     {
//         std::cout << iter << std::endl;
//     }
//     return 0;
// }
// int main()
// {
//     // 序列化
//     // 将结构化的数据转化为一个字符串
//     // Value 是一个Json的中间类,可以填充KV值
//     Json::Value root;
//     root["code"] = "mycode";
//     root["user"] = "dxl";
//     root["age"] = "23";

//     Json::StyledWriter writer;
//     std::string str = writer.write(root);

//     std::cout << str << std::endl;
//     // 反序列化
//     return 0;
// }

// void handler(int signo)
// {
//     std::cout << "sino : " << signo << std::endl;
//     _exit(2);
// }

// int main()
// {
//     // 资源不足,OS发送信号终止进程
//     for(int i= 1; i <= 31;i++)
//     {
//         signal(i, handler);
//     }
//     // 限制累计运行时长
//     struct rlimit r;
//     r.rlim_cur = 1;
//     r.rlim_max = RLIM_INFINITY;
//     setrlimit(RLIMIT_CPU, &r);
//     while(1);

//     // struct rlimit rt;
//     // rt.rlim_cur = 1024 * 1024 * 40;
//     // rt.rlim_max = RLIM_INFINITY;
//     // setrlimit(RLIMIT_AS, &rt);
//     // int count = 0;
//     // while(true)
//     // {
//     //     int * p = new int [1024 * 1024];
//     //     count++;
//     //     std::cout << "size: " << count << std::endl;
//     //     sleep(1);
//     // }
//     return 0;
// }
