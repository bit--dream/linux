#ifndef __POLL_SVR_H__
#define __POLL_SVR_H__

#include <iostream>
#include <string>
#include <poll.h>
#include <sys/time.h>
#include "Log.hpp"
#include "Sock.hpp"

#define FD_NONE -1
using namespace std;

class PollServer
{
private:
    static const int nfds = 1000;
    static const int timeout = 1000;

public:
    PollServer(const uint16_t &port = 8080)
        : _port(port),
          _nfds(nfds),
          _timeout(timeout)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        logMessage(DEBUG, "create base socket success");
        _fds = new pollfd[_nfds];
        for (int i = 0; i < _nfds; i++)
        {
            _fds[i].fd = FD_NONE;
            _fds[i].events = 0;
            _fds[i].revents = 0;
        }

        _fds[0].fd = _listensock;
        _fds[0].events = POLLIN;
    }

    void Start()
    {
        while (true)
        {
            int n = poll(_fds, _nfds, _timeout);

            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out ...");
                break;
            case -1:
                logMessage(WARNING, "poll error: %d : %s", errno, strerror(errno));
            default:
                // 成功
                logMessage(DEBUG, "get a new link event...");
                HandlerEvent();
                break;
            }
        }
    }
    ~PollServer()
    {
        if (_listensock >= 0)
            close(_listensock);

        if (_fds)
            delete[] _fds;
    }

private:
    void Accepter()
    {
        string clientip;
        uint16_t clientport = 0;
        // _listensock读事件就绪了,表示可以读取了
        // 获取新链接
        int sock = Sock::Accept(_listensock, &clientport, &clientip);
        if (sock < 0)
        {
            logMessage(WARNING, "accept error");
            return;
        }
        logMessage(DEBUG, "get a new link success : [%s:%d] : %d", clientip.c_str(), clientport, sock);

        int pos = 1;
        for (; pos < _nfds; pos++)
            if (_fds[pos].fd == FD_NONE)
                break;

        if (pos == _nfds)
        {
            logMessage(WARNING, "poll server already full, close: %d", sock);
            close(sock);
        }
        else
        {
            _fds[pos].fd = sock;
            _fds[pos].events = POLLIN;
        }
    }

    void Recver(int pos)
    {
        // input事件到来, recv, read
        logMessage(DEBUG, "message in, get IO event: %d", _fds[pos].fd);
        // 不能保证读取一个完整报文
        char buffer[1024];
        int n = recv(_fds[pos].fd, buffer, sizeof(buffer) - 1, 0);
        if (n > 0)
        {
            buffer[n] = 0;
            logMessage(DEBUG, "client[%d]# %s", _fds[pos].fd, buffer);
        }
        else if (n == 0)
        {
            logMessage(DEBUG, "client[%d] quit, me too...", _fds[pos].fd);
            // 1.关闭fd
            // 2.不要让select帮我关心当前的fd
            close(_fds[pos].fd);
            _fds[pos].fd == FD_NONE;
            _fds[pos].events = 0;
        }
        else
        {
            logMessage(WARNING, "%d sock recv error, %d : %s", _fds[pos].fd, errno, strerror(errno));
            close(_fds[pos].fd);
            _fds[pos].fd == FD_NONE;
            _fds[pos].events = 0;
        }
    }

    void HandlerEvent()
    {
        for (int i = 0; i < _nfds; i++)
        {
            if (_fds[i].fd == FD_NONE)
                continue;

            if (_fds[i].revents & POLLIN)
            {
                // 指定的fd, 读事件就绪
                if (i == 0 && _fds[i].fd == _listensock)
                    Accepter();
                else
                    Recver(i);
            }
        }
    }

    void DebugPrint()
    {
        cout << "_fds[].fd : ";
        for (int i = 0; i < _nfds; i++)
        {
            if (_fds[i].fd != FD_NONE)
                cout << _fds[i].fd << " ";
        }
        cout << endl;
    }

private:
    uint16_t _port;
    int _listensock;
    struct pollfd *_fds;
    nfds_t _nfds;
    int _timeout;
};

#endif
