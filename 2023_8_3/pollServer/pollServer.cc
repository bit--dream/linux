#include "pollServer.hpp"
#include <memory>
int main()
{   // fd_set fds;
    // std::cout << sizeof(fd_set) * 8 << std::endl;
    
    std::unique_ptr<PollServer> svr(new PollServer());
    svr->Start();
    return 0;
}