#pragma once
// 适配用户请求,定制通信协议字段
// 正确的调用compile and run
// 形成唯一的文件名

#include "compiler.hpp"
#include "runner.hpp"
#include "../comm/log.hpp"
#include "../comm/util.hpp"
#include <signal.h>
#include <jsoncpp/json/json.h>

namespace my_compile_and_run
{
    class CompileAndRun
    {
    public:
        /********************************************
         * code > 0 : 进程收到了信号导致异常崩溃
         * code < 0 : 整个过程非运行报错(代码为空, 编译报错等)
         * code == 0 : 整个过程全部完成
         **************************************/
        static std::string CodeToDesc(int status, const std::string &file_name)
        {
            std::string desc;
            switch (status)
            {
            case 0:
                desc = "编译运行成功";
                break;
            case -1:
                desc = "用户提交的代码为空";
                break;
            case -2:
                desc = "未知错误";
                break;
            case -3:
                my_util::FileUtil::ReadFile(my_util::PathUtil::CompilerError(file_name), &desc, true);
                break;
            case SIGABRT:
                desc = "内存超过范围";
                break;
            case SIGXCPU:
                desc = "CPU使用超时";
                break;
            case SIGFPE:
                desc = "除0/浮点数溢出";
                break;
            default:
                desc = "未知: " + std::to_string(status);
                break;
            }
            return desc;
        }

        // 信号转换为错误信息
        static std::string SignToDesc(int sign)
        {
        }

        /************************************************************************
         * 输入:
         * code:用户提交的代码
         * input:用户给自己提交的代码对应的输入
         * cpu_limit:时间要求
         * mem_limit:空间要求
         *
         * 输出:
         * 必填
         * status:状态码
         * reason:请求结果
         * 选填
         * stdout:我的程序进行完的结果
         * stderr:我的程序运行完的错误结果
         *
         * 参数:
         * in_json:{"code":"include...","input": "","cpu_limit":1, "mem_limit":10240}
         * out_json:{"status":"0","reason":"","stdout":"","stderr":""}
         * **************************************************************************/
        static void Start(const std::string &in_json, std::string *out_json)
        {
            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value);
            std::string code = in_value["code"].asString();
            std::string input = in_value["input"].asString();

            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            int status_code = 0;

            Json::Value out_value;
            int run_result = 0;
            std::string file_name; // 定义需要

            if (code.size() == 0)
            {
                // 差错处理
                status_code = -1; // 代码为空
                goto END;
                out_value["reason"] = "该用户提交的代码是空的";
                // 序列化
                // TODO
                return;
            }

            // 形成的文件名只具有唯一性,没有目录没有后缀
            // 毫秒级时间戳 + 原子性递增唯一值: 来保证唯一性
            file_name = my_util::FileUtil::UniqueFileName();

            // 将代码写到temp目录的唯一文件中
            // 形成临时src文件
            if (!my_util::FileUtil::WriteFile(my_util::PathUtil::Src(file_name), code))
            {
                status_code = -2;
                goto END;
                // 序列化
                return;
            }
            // 编译失败
            if (!my_compiler::Compiler::Compile(file_name))
            {
                status_code = -3;
                goto END;

                // 序列化
                return;
            }

            run_result = my_runner::Runner::Run(file_name, cpu_limit, mem_limit);
            if (run_result < 0) // 内部错误
            {
                status_code = -2;
                // 序列化
                return;
            }
            else if (run_result > 0) // 运行时错误
            {
                status_code = run_result;
            }
            else
            {
                // 运行成功
                status_code = 0;
            }
        END:
            out_value["status"] = status_code;
            out_value["reason"] = CodeToDesc(status_code, file_name);
            if (status_code == 0)
            {
                // 整个过程全部成功
                std::string _stdout;
                my_util::FileUtil::ReadFile(my_util::PathUtil::Stdout(file_name), &_stdout, true);
                out_value["stdout"] = _stdout;
                std::string _stderr;
                my_util::FileUtil::ReadFile(my_util::PathUtil::Stderr(file_name), &_stderr, true);
                out_value["stderr"] = _stderr;
            }
            else
            {
            }

            Json::StyledWriter write;
            *out_json = write.write(out_value);
        }
    };
}
