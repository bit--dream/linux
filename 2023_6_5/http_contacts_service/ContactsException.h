#include <iostream>
#include <string>

class ContactsException
{
private:
    std::string _message;
public:
    ContactsException(std::string str = "A problem") : _message(str)
    {}

    std::string what() const 
    {
        return _message;
    }
    ~ContactsException(){}
};

