#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <unistd.h>

int main()
{
    printf("当前进程的开始代码\n");
    //execl("/usr/bin/ls", "ls", "-l", NULL);
    //execl("/usr/bin/ls", "ls", "--color=auto", "-l", NULL);
    //execl("/usr/bin/ls", "ls", "-a", "-l", NULL);
    //execl("/usr/bin/top", "top", NULL);
    execl("/usr/bin/msnas", "msnas", NULL);
    exit(1);
    printf("当前进程的结束代码\n");

    return 0;
}

//
//typedef void (*handler_t)(); //函数指针类型
//
//std::vector<handler_t> handlers; //函数指针数组
//
//void fun_one()
//{
//    printf("这是一个临时任务1\n");
//}
//
//void fun_two()
//{
//    printf("这是一个临时任务2\n");
//}
//
//// 设置对应的方法回调
//// 以后想让父进程闲了执行任何方法的时候，只要向Load里面注册，就可以让父进程执行对应的方法喽!
//void Load()
//{
//    handlers.push_back(fun_one);
//    handlers.push_back(fun_two);
//}
//
//int main()
//{
//    pid_t id = fork();
//    if(id == 0)
//    {
//        // 子进程
//        int cnt =  5;
//        while(cnt)
//        {
//            printf("我是子进程: %d\n", cnt--);
//            sleep(1);
//        }
//
//        exit(11); // 11 仅仅用来测试
//    }
//    else
//    {
//        int quit = 0;
//        while(!quit)
//        {
//            int status = 0;
//            pid_t res = waitpid(-1, &status, WNOHANG); //以非阻塞方式等待
//            if(res > 0)
//            {
//                //等待成功 && 子进程退出
//                printf("等待子进程退出成功, 退出码: %d\n", WEXITSTATUS(status));
//                quit = 1;
//            }
//            else if( res == 0  )
//            {
//                //等待成功 && 但子进程并未退出
//                printf("子进程还在运行中，暂时还没有退出，父进程可以在等一等, 处理一下其他事情？？\n");
//                if(handlers.empty()) Load();
//                for(auto iter : handlers)
//                {
//                    //执行处理其他任务
//                    iter();
//                }
//            }
//            else
//            {
//                //等待失败
//                printf("wait失败!\n");
//                quit = 1;
//            }
//            sleep(1);
//        }
//    }
//    return 0;
//}
//
