#include <iostream>
#include <fstream>
#include "contacts.pb.h"

using namespace std;

void AddPeopleInfo(contacts2::PeopleInfo *people)
{
    cout << "-------------新增联系⼈-------------" << endl;

    cout << "请输⼊联系⼈姓名: ";
    string name;
    getline(cin, name);
    people->set_name(name);

    cout << "请输⼊联系⼈年龄: ";
    int age;
    cin >> age;
    people->set_age(age);

    cin.ignore(256, '\n'); // 清除输入缓冲区的内容

    for (int i = 1;; i++)
    {
        cout << "请输⼊联系⼈电话" << i << "(只输⼊回⻋完成电话新增): ";
        string number;
        getline(cin, number);
        if (number.empty())
            break;

        contacts2::PeopleInfo_Phone *phone = people->add_phone();
        phone->set_number(number);

        cout << "请输入该电话类型(1.移动电话 2.固定电话):";
        int type;
        cin >> type;
        cin.ignore(256, '\n');
        switch (type)
        {
        case 1:
            phone->set_type(contacts2::PeopleInfo_Phone_PhoneType::PeopleInfo_Phone_PhoneType_MP);
            break;
        case 2:
            phone->set_type(contacts2::PeopleInfo_Phone_PhoneType::PeopleInfo_Phone_PhoneType_TML);
            break;
        default:
            cout << "选择有误!" << endl; 
            break;
        }
    }

    contacts2::Address address;
    cout << "请输入联系人家庭住址: ";
    string home_address;
    getline(cin, home_address);
    address.set_home_address(home_address);
    cout <<  
    
    cout << "-----------添加联系⼈成功-----------" << endl;
}

int main()
{
    contacts2::Contacts contacts;

    // 读取本地已存在的通讯录文件
    fstream input("contacts.bin", ios::in | ios::binary);
    if (!input)
    {
        cout << "contacts.bin not find, reate new file!" << endl;
    }
    else if (!contacts.ParseFromIstream(&input))
    {
        cout << "parse error!" << endl;
        input.close();
        return -1;
    }
    // 向通讯录添加一个联系人
    AddPeopleInfo(contacts.add_contacts());
    // 将通讯录写入本地文件中
    fstream output("contacts.bin", ios::out | ios::trunc | ios::binary);
    if (!contacts.SerializeToOstream(&output))
    {
        cerr << "write error!" << endl;
        input.close();
        output.close();

        return -1;
    }
    cout << "wrie success!" << endl;
    input.close();
    output.close();
    return 0;
}