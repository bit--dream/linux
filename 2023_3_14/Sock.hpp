#pragma once
#include <iostream>
#include <string>
#include <unordered_map>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <algorithm>
#include <signal.h>
#include <memory>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <pthread.h>
#include "Log.hpp"

const int SIZE = 1024;

class Sock
{
private:
    const static int gbacklog = 20;

public:
    Sock()
    {
    }

    int Socket()
    {
        // 1.创建套接字
        int listenSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (listenSocket < 0)
        {
            logMessage(FATAL, "create socket error, %d : %s", errno, strerror(errno));
            exit(2);
        }
        int opt = 1;
        setsockopt(listenSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
        logMessage(NORMAL, "create socket success, listenSocket: %d", listenSocket); // 3
        return listenSocket;
    }

    void Bind(int sock, const uint16_t &port, const std::string &ip = "0.0.0.0")
    {
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);

        local.sin_family = AF_INET;
        inet_pton(AF_INET, ip.c_str(), &local.sin_addr);
        local.sin_port = htons(port);

        if (bind(sock, (struct sockaddr *)&local, sizeof local) < 0)
        {
            logMessage(FATAL, "bind error, %d : %s", errno, strerror(errno));
            exit(3);
        }
    }

    void Listen(int sock)
    {
        if (listen(sock, gbacklog) < 0)
        {
            logMessage(FATAL, "listen error, %d : %s", errno, strerror(errno));
            exit(4);
        }
        logMessage(NORMAL, "init server success!\n");
    }

    // const std::string &:输入型参数
    // std::string *:输出型参数
    // std::string &:输入输出型参数
    int Accept(int listensock, uint16_t *port, std::string *ip)
    {
        struct sockaddr_in temp;
        socklen_t len = sizeof temp;

        int service_socket = accept(listensock, (struct sockaddr *)&temp, &len);
        if (service_socket < 0)
        {
            logMessage(ERROR, "accept error, %d : %s", errno, strerror(errno));
            return -1;
        }

        if (port)
            *port = ntohs(temp.sin_port);
        if (ip)
            *ip = inet_ntoa(temp.sin_addr);
        return service_socket;
    }

    bool Connect(int sock, const std::string &server_ip, const uint16_t &server_port)
    {
        struct sockaddr_in server;
        memset(&server, 0, sizeof server);
        
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(server_ip.c_str());
        server.sin_port = htons(server_port);
        if (connect(sock, (struct sockaddr *)&server, sizeof server) == 0)
            return true;
        else
            return false;
    }

    ~Sock() {}
};
