#include "Sock.hpp"


int main()
{
    Sock sock;
    int listensock = sock.Socket();
    sock.Bind(listensock, 8080);

    sock.Listen(listensock);

    std::string clientip;
    uint16_t clientport;
    while (true)
    {
        int sockfd = sock.Accept(listensock, &clientport, &clientip);
        if(sockfd > 0)
        {
            std::cout << "[" << clientip << ":" << clientport << "]#" << std::endl;
        }
        sleep(10);
        close(sockfd);
        
        std::cout << sockfd << std::endl;
    }
    
    return 0;
}