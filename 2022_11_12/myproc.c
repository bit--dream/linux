#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <unistd.h>

int main()
{
    pid_t id = fork();

    if(id == 0) 
    {
        // 子进程
        int cnt =  5;
        while(cnt)
        {
            printf("我是子进程: %d\n", cnt--);
            sleep(1);
        }

        exit(11); // 11 仅仅用来测试
    }
    else 
    {
        int quit = 0;
        while(!quit)
        {
            int status = 0;
            pid_t res = waitpid(-1, &status, WNOHANG);//非阻塞等待
            if(res > 0)
            {
                //等待成功
                printf("等待子进程退出成功,退出码:%d\n", WEXITSTATUS(status));
                quit = 1;
            }
            else if(res == 0)
            {
                //等待成功，但子进程并未退出
                printf("子进程还在运行中，暂时还没有退出，父进程可以在等一等, 处理一下其他事情？？\n");
            }
            else 
            {
                //等待失败
                printf("wait失败!\n");
                quit = 1;
            }
            sleep(1);
        }
    }
      //// 父进程
  //  else 
  //  {

  // int status = 0;
  // // 只有子进程退出的时候，父进程才会waitpid函数，进行返回！！[父进程依旧还活着呢！！]
  // // waitpid/wait 可以在目前的情况下，让进程退出具有一定的顺序性！
  // // 将来可以让父进程进行更多的收尾工作.
  // // id > 0 : 等待指定进程
  // // id == -1: 等待任意一个子进程退出, 等价于wait();
  // // options: 默认为0，代表阻塞等待, WNOHANG: 叫做非阻塞等待
  // pid_t result = waitpid(id, &status, 0); // 默认是在阻塞状态去等待子进程状态变化(就是退出!)
  // if(result > 0)
  // {
  //     // 可以不这么检测
  //     //printf("父进程等待成功, 退出码: %d, 退出信号： %d\n", (status>>8)&0xFF, status & 0x7F);
  //     if(WIFEXITED(status))
  //     {
  //         //子进程是正常退出的
  //         printf("子进程执行完毕,子进程的退出码: %d\n", WEXITSTATUS(status));
  //     }
  //     else{
  //         printf("子进程异常退出: %d\n", WIFEXITED(status));
  //       }
  //  }
  //  }
    return 0;
}
