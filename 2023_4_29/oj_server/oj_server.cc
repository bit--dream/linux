#include <iostream>
#include "../comm/httplib.h"


int main()
{
    // 用户请求的服务路由功能
    httplib::Server svr;

    // 获取所有的题目列表
    svr.Get("/all_questions", [](const httplib::Request &req, httplib::Response &resp){
        resp.set_content("这是所有题目的列表", "text/plain;charset=utf-8");
    });
    // 用户要根据题目编号, 获取题目的内容
    // /questions/100 -> 正则匹配
    // R"()" 原始字符串,保持字符串内容的原貌
    svr.Get(R"(/question/(\d+))", [](const httplib::Request &req, httplib::Response &resp){
        std::string number = req.matches[1];
        resp.set_content("这是指定的一道题: " + number, "text/plain;charset=utf-8");
    });

    // 用户提交代码,使用我们的判题功能(1.每道题的测试用例 2.compile_and_run)
    svr.Get(R"(/judge/(\d+))", [](const httplib::Request &req, httplib::Response &resp){
        std::string number = req.matches[1];
        resp.set_content("指定题目的判题: " + number, "text/plaiin;charset=utf-8");
    });
    svr.set_base_dir("./wwwroot");
    svr.listen("0.0.0.0", 8080);
    std::cout << "" << std::endl;
    return 0;
}