#pragma once

#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <queue>

#include "LockGuard.hpp"
using namespace std;

const int gDefaultCap = 5;

template <class T>
class BlockQueue
{
private:
    bool isQueueEmpty()
    {
        return _bq.size() == 0;
    }

    bool isQueueFull()
    {
        return _bq.size() == _capacity;
    }

public:
    BlockQueue(int capacity = gDefaultCap)
        : _capacity(capacity)
    {
        pthread_mutex_init(&_mtx, nullptr);
        pthread_cond_init(&_Empty, nullptr);
        pthread_cond_init(&_Full, nullptr);
    }

    void push(const T &in) // 生产者
    {
        // pthread_mutex_lock(&_mtx);
        // // 1. 先检测当前的临界资源是否能够满足访问条件
        // // pthread_cond_wait第二个参数是一把锁, 当调用wait以后, 传入的锁会自动释放

        // while (isQueueFull())
        // {
        //     pthread_cond_wait(&_Full, &_mtx);
        // }
        // // 2. 访问临界资源
        // _bq.push(in);
        // // if (_bq.size() >= _capacity / 2)
        // pthread_cond_signal(&_Empty);
        // pthread_mutex_unlock(&_mtx);

        LockGuard lockGuard(&_mtx);
        while (isQueueFull())
        {
            pthread_cond_wait(&_Full, &_mtx);
        }
        // 2. 访问临界资源
        _bq.push(in);
        // if (_bq.size() >= _capacity / 2)
        pthread_cond_signal(&_Empty);
        
    }

    void pop(T *out) // 消费者
    {
        LockGuard lockGuard(&_mtx);
        // pthread_mutex_lock(&_mtx);
        while (isQueueEmpty())
        {
            pthread_cond_wait(&_Empty, &_mtx);
        }
        *out = _bq.front();
        _bq.pop();
        // pthread_mutex_unlock(&_mtx);
        pthread_cond_signal(&_Full);
    }

    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mtx);
        pthread_cond_destroy(&_Empty);
        pthread_cond_destroy(&_Full);
    }

private:
    queue<T> _bq;          // 阻塞队列
    int _capacity;         // 容量上限
    pthread_mutex_t _mtx;  // 通过互斥锁保证队列的安全
    pthread_cond_t _Empty; // 用它来表示队列是否空的条件
    pthread_cond_t _Full;  // 用它来表示队列是否满的条件
};
