#ifndef __SELECT_SVR_H__
#define __SELECT_SVR_H__

#include <iostream>
#include <string>
#include <sys/select.h>
#include <sys/time.h>
#include "Log.hpp"
#include "Sock.hpp"

#define BITS 8
#define NUM (sizeof(fd_set) * BITS)
#define FD_NONE -1
using namespace std;

class SelectServer
{
public:
    SelectServer(const uint16_t &port = 8080)
        : _port(port)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        logMessage(DEBUG, "create base socket success");
        for (int i = 0; i < NUM; i++)
            _fdarr[i] = FD_NONE;

        _fdarr[0] = _listensock;
    }

    void Start()
    {

        while (true)
        {
            DebugPrint();

            fd_set rdfs;
            FD_ZERO(&rdfs);
            int maxfd = _listensock;

            for (int i = 0; i < NUM; i++)
            {
                if (_fdarr[i] == FD_NONE)
                    continue;
                FD_SET(_fdarr[i], &rdfs);

                if (_fdarr[i] > maxfd)
                    maxfd = _fdarr[i];
            }
            // struct timeval timeout = {5, 0};
            // 将_listensock添加到读文件描述符集中

            int n = select(maxfd + 1, &rdfs, nullptr, nullptr, nullptr);
            // int n = select(_listensock + 1, &rdfs, nullptr, nullptr, &timeout);

            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out ...");
                break;
            case -1:
                logMessage(WARNING, "select error: %d : %s", errno, strerror(errno));
            default:
                // 成功
                logMessage(DEBUG, "get a new link event...");
                HandlerEvent(rdfs);
                break;
            }
        }
    }
    ~SelectServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    void Accepter()
    {
        string clientip;
        uint16_t clientport;

        // _listensock读事件就绪了,表示可以读取了
        // 获取新链接
        int sock = Sock::Accept(_listensock, &clientport, &clientip);
        if (sock < 0)
        {
            logMessage(WARNING, "accept error");
            return;
        }
        logMessage(DEBUG, "get a new link success : [%s:%d] : %d", clientip.c_str(), clientport, sock);

        int pos = 1;
        for (; pos < NUM; pos++)
            if (_fdarr[pos] == FD_NONE)
                break;

        if (pos == NUM)
        {
            logMessage(WARNING, "select server already full, close: %d", sock);
            close(sock);
        }
        else
            _fdarr[pos] = sock;
    }

    void Recver(int pos)
    {
        // input事件到来, recv, read
        logMessage(DEBUG, "message in, get IO event: %d", _fdarr[pos]);
        // 不能保证读取一个完整报文
        char buffer[1024];
        int n = recv(_fdarr[pos], buffer, sizeof(buffer) - 1, 0);
        if (n > 0)
        {
            buffer[n] = 0;
            logMessage(DEBUG, "client[%d]# %s", _fdarr[pos], buffer);
        }
        else if (n == 0)
        {
            logMessage(DEBUG, "client[%d] quit, me too...", _fdarr[pos]);
            // 1.关闭fd
            // 2.不要让select帮我关心当前的fd
            close(_fdarr[pos]);
            _fdarr[pos] == FD_NONE;
        }
        else
        {
            logMessage(WARNING, "%d sock recv error, %d : %s", _fdarr[pos], errno, strerror(errno));
            close(_fdarr[pos]);
            _fdarr[pos] == FD_NONE;
        }
    }
    
    void HandlerEvent(const fd_set &rfds)
    {
        for (int i = 0; i < NUM; i++)
        {
            if (_fdarr[i] == FD_NONE)
                continue;

            if (FD_ISSET(_fdarr[i], &rfds))
            {
                // 指定的fd, 读事件就绪
                if (i == 0 && _fdarr[i] == _listensock) Accepter();
                else Recver(i);
            }
        }
    }

    void DebugPrint()
    {
        cout << "_fdarr[] : ";
        for (int i = 0; i < NUM; i++)
        {
            if (_fdarr[i] != FD_NONE)
                cout << _fdarr[i] << " ";
        }
        cout << endl;
    }

private:
    uint16_t _port;
    int _listensock;
    int _fdarr[NUM];
};

#endif