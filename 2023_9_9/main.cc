#include <iostream>

using namespace std;

class A
{
public:
    A() 
    {
        cout << "0";
    }
    A(const A& a) 
    {
        cout << "1";
    }
    A operator=(const A& a)
    {
        cout << "2";
    }
    ~A() 
    {
        cout << "3";
    }
};

A func()
{
    A a;
    return a;
}
int main()
{
    A b;
    b = func();
    return 0;
}
