#include <iostream>
#include <cstdlib>
#include <string>
#include <mysql.h>

using namespace std;

string host = "127.0.0.1";
string user = "dxl";
string passwd = "20030203";
string db = "dxl";
unsigned int port = 8080;


int main()
{
    // cout << "mysql client version: " << mysql_get_client_info() << endl;
    // 0. 初始化mysql对象
    MYSQL *mysql = mysql_init(nullptr);
    if(mysql == nullptr)
    {
        cout << "mysql_init error" << endl;
        exit(1);
    }
    // 1. 登录认证
    if(mysql_real_connect(mysql, host.c_str(), user.c_str(), passwd.c_str(), db.c_str(), port, nullptr, 0) == nullptr)
    {
        cerr << "mysql_real_connect error" << endl;
        exit(2);
    }
    
    cout << "mysql_real_connect success" << endl;

    
    // 关闭数据库
    mysql_close(mysql);
    return 0;
}