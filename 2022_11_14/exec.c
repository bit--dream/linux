#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

#define NUM 16

//const char* myfile = "/home/dxl/linux/2022_11_14/mycmd";
const char* myfile = "./mycmd";

int main(int argc, char* argv[], char* env[])
{
    char* const _env[NUM] = {
        (char*)"my=5555",
        NULL
    };
    pid_t id = fork();
    if(id == 0)
    {
        //子进程
        printf("子进程开始执行,pid: %d\n", getpid());
        sleep(3);

        //execlp("ls", "ls", "-a", "-l", NULL);

        char* const _argv[NUM] = {(char*)"ls", (char*)"-a", (char*)"-l", (char*)"-i", NULL};

        execle(myfile, "mycmd", "-a", NULL, _env);


        //execlp("python", "python", "test.py", NULL);
        //execv("/usr/bin/ls", _argv);
        //execl("/usr/bin/ls", "ls", "-l", "-a", NULL);
        //execvp("ls", _argv);
        execl(myfile, "mycmd", "-a", NULL);
        exit(1);
    }
    else 
    {
        //父进程
        printf("父进程开始执行,pid: %d\n", getpid());
        int status = 0;
        pid_t ret = waitpid(-1, &status, 0);//阻塞等待
        if(ret > 0)
        {
            printf("等待子进程退出成功,exit cod: %d\n", WEXITSTATUS(status));
        }
    }
    return 0;
}


