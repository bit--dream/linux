#pragma once
#include <iostream>
#include <sys/epoll.h>

class Epoll
{
public:
    static const int g_num = 128;
    static const int g_timeout = 5000;
public:
    Epoll(int timeout = g_timeout)
        : _timeout(timeout)
    {}
    
    void CreatEpoll()
    {
        _epfd = epoll_create(g_num);
        if (_epfd < 0)
            exit(5);
    }
    bool AddSockToEpoll(int sock, uint32_t events)
    {
        struct epoll_event ev;
        ev.data.fd = sock;
        ev.events = events;
        int n = epoll_ctl(_epfd, EPOLL_CTL_ADD, sock, &ev);
        return n == 0;
    }
    int WaitEpoll(struct epoll_event recvs[], int num)
    {
        // std::cout << "waitepoll" << std::endl;
        return epoll_wait(_epfd, recvs, num, _timeout);
    }
    ~Epoll()
    {
        
    }

private:
    int _timeout;
    int _epfd;
};
