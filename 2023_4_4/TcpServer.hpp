#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <unordered_map>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"

// using namespace std;

class TcpServer;
class Connection;

using func_t = std::function<void(Connection *)>;

class Connection
{

public:
    Connection(int sock = -1)
        : _sock(sock),
          _tsvr(nullptr)
    {
    }
    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    int _sock; // 负责进行IO的文件描述符
    // 三个回调方法,表征的是对_sock进行特定读写对应的方法
    func_t _recv_cb;   // 读
    func_t _send_cb;   // 写
    func_t _except_cb; // 异常
    // 接收缓冲区 && 发送缓冲区
    std::string _inbuffer; // 暂时没有办法处理二进制流, 文本是可以的
    std::string _outbuffer;

    // 设置对TCPServer的回值指针
    TcpServer *_tsvr;
};

class TcpServer
{
    static const uint16_t g_port = 8080;
    static const int g_num = 128;

public:
    TcpServer(uint16_t port = g_port)
        : _port(port),
          _recvs_num(g_num)
    {
        // 1. 创建listensock
        _listenSock = Sock::Socket();
        Sock::Bind(_listenSock, _port);
        Sock::Listen(_listenSock);

        // 2. 创建多路转接对象
        _poll.CreatEpoll();

        // 3. 添加listensock到服务器中
        AddConnection(_listenSock, std::bind(&TcpServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);

        // 4. 构建一个获取就绪事件的缓冲区
        _recvs = new struct epoll_event[_recvs_num];
    }

    // 专门针对任意sock进行添加TcpServer
    void AddConnection(int sock, func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        Sock::SetNonBlock(sock); // 设置非阻塞

        // 除了_listensock, 未来我们会存在大量的socket,每一个sock都必须被封装成一个Connection
        // 有大量的Connection时,TcpServer就需要将所有的Connection进行管理
        // 1. 构建conn对象,封装sock
        Connection *conn = new Connection(sock);
        conn->SetCallBack(recv_cb, send_cb, except_cb);
        conn->_tsvr = this;

        // 2. 添加sock[]到epoll中
        // 任何多路转接服务器,一般只会默认打开读取事件的关心,写入事件会按需进行打开
        _poll.AddSockToEpoll(sock, EPOLLIN | EPOLLET);
        // 3. 还要将对应的Connection*对象指针添加到Connection映射表中
        _connections.insert(std::make_pair(sock, conn));
        // std::cout << "添加成功" << std::endl;
    }
    void Accepter(Connection *conn)
    {
        // logMessage(DEBUG, "Accepter been called");
        // 一定是listensock就绪,此次读取不会阻塞
        while (true)
        {
            std::string clientip;
            uint16_t clientport;
            int accept_errno = 0;
            int sock = Sock::Accept(conn->_sock, &clientport, &clientip, &accept_errno);
            // sock一定是常规的IOserver
            if (sock < 0)
            {
                if (accept_errno == EAGAIN || accept_errno == EWOULDBLOCK)
                    break; // 没有链接
                else if (accept_errno == EINTR)
                    continue; // 信号中断
                else
                {
                    // accept 失败
                    logMessage(WARNING, "accept error, %d : %s", accept_errno, strerror(accept_errno));
                    break;
                }
            }
            if (sock >= 0)
            {
                // 将sock托管给TcpServer
                AddConnection(sock, std::bind(&TcpServer::Recver, this, std::placeholders::_1),
                              std::bind(&TcpServer::Recver, this, std::placeholders::_1),
                              std::bind(&TcpServer::Recver, this, std::placeholders::_1));
                logMessage(DEBUG, "accept client %s : %d success, add to epoll && TcpServer success, sock : %d",\
                                    clientip.c_str(), clientport, sock);
            }
        }
    }

    void Recver(Connection *conn)
    {
        logMessage(DEBUG, "Recver event exists, Recver() been called");
    }
    void Sender(Connection *conn)
    {
    }
    void Excepter(Connection *conn)
    {
    }

    void LoopOnce()
    {
        int n = _poll.WaitEpoll(_recvs, _recvs_num);
        // std::cout << n << std::endl;
        for (int i = 0; i < n; i++)
        {
            int sock = _recvs[i].data.fd;
            uint32_t revent = _recvs[i].events;
            if (revent & EPOLLIN)
            {
                // std::cout << "读取" << std::endl;
                // 一旦有读事件就绪,就会调用回调函数
                if (_connections.count(sock) && _connections[sock]->_recv_cb != nullptr)
                    _connections[sock]->_recv_cb(_connections[sock]);
            }
            if (revent & EPOLLOUT)
            {
                // std::cout << "写入" << std::endl;

                if (_connections.count(sock) && _connections[sock]->_send_cb != nullptr)
                    _connections[sock]->_send_cb(_connections[sock]);
            }
        }
    }
    // bool IsConnectionExist(int sock)
    // {
    //     auto iter = _connections.find(sock);
    //     if (iter == _connections.end())
    //         return false;
    //     else
    //         return true;
    // }
    // 根据就绪的事件,进行特定事件的派发
    void Dispather()
    {
        while (true)
        {
            LoopOnce();
        }
    }
    ~TcpServer()
    {
        if (_listenSock >= 0)
            close(_listenSock);

        if (_recvs)
            delete[] _recvs;
    }

private:
    int _listenSock;
    uint16_t _port;
    Epoll _poll;
    // sock : Connection
    std::unordered_map<int, Connection *> _connections;
    struct epoll_event *_recvs;
    int _recvs_num;
};
