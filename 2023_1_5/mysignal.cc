#include <iostream>
#include <unistd.h>
#include <assert.h>
#include <signal.h>

static void showPending(sigset_t &pending)
{
    for (int i = 1; i <= 31; i++)
    {
        if (sigismember(&pending, i))
            std::cout << "1";
        else
            std::cout << "0";
    }
    std::cout << std::endl;
}

static void handler(int signum)
{
    std::cout << "捕捉 信号 : " << signum << std::endl;
}

static void blockSig(int signum)
{
    sigset_t bset;
    sigemptyset(&bset);
    sigaddset(&bset, signum);
    int n = sigprocmask(SIG_BLOCK, &bset, nullptr);
    assert(n == 0);
    (void)n;
}

int main()
{
    for (int sig = 1; sig <= 31; sig++)
    {
        blockSig(sig);
    }
    sigset_t pending;
    while (true)
    {
        sigpending(&pending);
        showPending(pending);
        sleep(1);
    }

    // //捕捉2号信号
    // signal(2, handler);
    // // 1.定义信号集对象
    // sigset_t bset, obset;
    // sigset_t pending;

    // // 2.初始化
    // sigemptyset(&bset);
    // sigemptyset(&obset);
    // sigemptyset(&pending);

    // // 3.要添加进行屏蔽的信号
    // sigaddset(&bset, 2);

    // // 4.设置set到内核中对应的进程内部
    // int n = sigprocmask(SIG_BLOCK, &bset, &obset);
    // assert(n == 0);
    // (void)n;
    // std::cout << "block 2号 信号成功..., pid : " << getpid() << std::endl;

    // // 5.重新打印当前进程的pending信号集
    // int cnt = 0;
    // while (true)
    // {
    //     // 获取当前进程的pending信号集
    //     sigpending(&pending);
    //     // 没有在pending信号集中的信号
    //     showPending(pending);
    //     sleep(1);
    //     cnt++;
    //     if(cnt == 30)
    //     {
    //         int n = sigprocmask(SIG_SETMASK, &obset, nullptr);
    //         assert(n == 0);
    //         (void)n;
    //         std::cout << "解除对2号信号的block" << std::endl;
    //     }
    // }

    return 0;
}

// void catchSig(int signum)
// {
//     std::cout << "获得了一个信号" << signum << std::endl;

// }

// int main()
// {
//     for(int i = 1; i <= 31; i++)
//     {
//         signal(i, catchSig);
//     }
//     // signal(2, SIG_DFL);// 默认行为
//     // signal(2, SIG_IGN);// 忽略

//     while(true)
//     {
//         sleep(1);
//     }

//     return 0;
// }