#pragma once

#include <iostream>
#include <string>
#include <ctemplate/template.h>

// #include "oj_model.hpp"
#include "oj_model2.hpp"

namespace my_view
{
    // struct Question
    // {
    //     std::string number; // 题目编号,唯一
    //     std::string title;  // 题目的标题
    //     std::string star;   // 难度: 简单 中等 困难
    //     int cpu_limit;      // 题目的时间要求
    //     int mem_limit;      // 题目的空间要求
    //     std::string decs;   // 题目的描述
    //     std::string header; // 题目预设给用户在线编辑器的代码
    //     std::string tail;   // 题目的测试用例,需要和header拼接,形成完整代码
    // };

    const std::string template_path = "./template_html/";
    class View
    {
    public:
        View()
        {
        }
        ~View()
        {
        }

    public:
        void AllExpandHtml(const std::vector<my_model::Question> &questions, std::string *html)
        {
            // 题目的编号 题目的标题 题目的难度
            // 1. 形成路径
            std::string src_html = template_path + "all_questions.html";
            // 2. 形成数据字典
            ctemplate::TemplateDictionary root("all_questions");

            for (const auto &q : questions)
            {
                ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("question_list");
                sub->SetValue("number", q.number);
                sub->SetValue("title", q.title);
                sub->SetValue("star", q.star);
            }
            // 3. 获取被渲染的html
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            // 4. 开始完成渲染功能
            tpl->Expand(html, &root);
        }

        void OneExpandHtml(const my_model::Question &q, std::string *html)
        {
            // 1. 形成路径
            std::string src_html = template_path + "one_question.html";

            // 2. 形成数据字典
            ctemplate::TemplateDictionary root("one_question");

            // std::cout << q.header << std::endl;
            root.SetValue("number", q.number);
            root.SetValue("title", q.title);
            root.SetValue("star", q.star);
            root.SetValue("desc", q.desc);
            root.SetValue("pre_code", q.header);

            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            tpl->Expand(html, &root);
        }

        static void LoginExpandHtml(std::string *html)
        {
            // 1. 形成路径
            std::string src_html = template_path + "login.html";

            // 2. 形成数字典
            ctemplate::TemplateDictionary root("login");

            // 3. 获取被渲染的html
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            // 4. 开始完成渲染功能
            tpl->Expand(html, &root);
        }

        static void CompetitionExpandHtml(std::string *html)
        {
            // 1. 形成路径
            std::string src_html = template_path + "competition.html";

            // 2. 形成数字典
            ctemplate::TemplateDictionary root("competition");

            // 3. 获取被渲染的html
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            // 4. 开始完成渲染功能
            tpl->Expand(html, &root);
        }

        static void TalkExpandHtml(std::string *html)
        {
            // 1. 形成路径
            std::string src_html = template_path + "talk.html";

            // 2. 形成数字典
            ctemplate::TemplateDictionary root("talk");

            // 3. 获取被渲染的html
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            // 4. 开始完成渲染功能
            tpl->Expand(html, &root);
        }

        static void JobExpandHtml(std::string *html)
        {
            // 1. 形成路径
            std::string src_html = template_path + "job_hunt.html";

            // 2. 形成数字典
            ctemplate::TemplateDictionary root("job_hunt");

            // 3. 获取被渲染的html
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            // 4. 开始完成渲染功能
            tpl->Expand(html, &root);
        }
    };
}
