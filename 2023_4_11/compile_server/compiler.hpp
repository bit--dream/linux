#pragma once
// 只负责代码的编译

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../comm/util.hpp"
#include "../comm/log.hpp"

namespace my_compiler
{
    class Compiler
    {
    public:
        Compiler() {}
        ~Compiler() {}
        // 返回值: 编译成功:true 否则: false
        // 输入参数: 编译的文件名
        // xxx -> ./temp/xxx.cc
        // xxx -> ./temp/xxx.exe
        // xxx -> ./temp/xxx.stderr
        static bool Compile(const std::string &file_name)
        {
            pid_t id = fork();
            if (id < 0)
            {
                my_log::LOG(ERROR) << "内部错误, 创建子进程失败" << "\n";
                return false;
            }
            else if (id == 0)
            {
                umask(0);
                int std_err = open(my_util::PathUtil::CompilerError(file_name).c_str(), O_CREAT | O_WRONLY, 0644);
                if(std_err < 0)
                {
                    my_log::LOG(WARNING) << "没有成功形成compile_error文件" << "\n";
                    exit(1);
                }
                // 重定向标准错误到str_err
                dup2(std_err,2);
                // 子进程 : 调用编译器,对代码进行编译
                execlp("g++", "g++", "-o", my_util::PathUtil::Exe(file_name).c_str(), \
                my_util::PathUtil::Src(file_name).c_str(), "-std=c++11", nullptr);
                my_log::LOG(ERROR) << "启动编译器g++失败,可能是参数错误" << "\n";
                exit(2);
            }
            else
            {
                waitpid(id, nullptr, 0);
                // 编译是否成功,就看有没有形成对应的可执行程序
                if(my_util::FileUtil::IsFileExists(my_util::PathUtil::Exe(file_name)))
                {
                    my_log::LOG(INFO) << my_util::PathUtil::Src(file_name) << " 编译成功!" << "\n"; 
                    return true;
                }
            }
            my_log::LOG(DEBUG) << my_util::PathUtil::Src(file_name) << "\n";
            my_log::LOG(DEBUG) << "编译失败,没有形成可执行程序" << "\n";
            return false;
        }
    };
}