#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "../comm/log.hpp"
#include "../comm/util.hpp"
namespace my_runner
{
    class Runner
    {
    public:
        Runner() {}
        ~Runner() {}

        // 指明文件名即可, 不需要带路径,不需要带后缀
        /**************************
         * 返回值 > 0 : 程序异常,退出时收到了信号,返回值就是对应信号编号
         * 返回值 == 0 : 正常执行完的,结果保存到了对应的临时文件中
         * 返回值 < 0 : 内部错误
         * *************************/
        static int Run(const std::string &file_name)
        {
            /********************************
             * 程序运行:
             * 1. 代码跑完,结果正确
             * 2. 代码跑完,结果不正确
             * 3. 代码没跑完,异常
             * Run不需要考虑代码跑完,结果正确不正确
             * 结果正确与否:由我们的测试用例决定的
             * 我们只考虑: 是否正确运行完毕
             * 我们必须知道可执行程序是谁
             * 一个程序在默认启动:
             * 标准输入: 不处理
             * 标准输出: 程序运行完成,输出结果是什么
             * 标准错误: 运行时错误信息
             * ****************************************/

            std::string exe_cute = my_util::PathUtil::Exe(file_name);
            std::string std_in = my_util::PathUtil::Stdin(file_name);
            std::string std_out = my_util::PathUtil::Stdout(file_name);
            std::string std_err = my_util::PathUtil::Stderr(file_name);

            umask(0);
            int std_in_fd = open(std_in.c_str(), O_CREAT | O_RDONLY, 0644);
            int std_out_fd = open(std_out.c_str(), O_CREAT | O_WRONLY, 0644);
            int std_err_fd = open(std_err.c_str(), O_CREAT | O_WRONLY, 0644);

            if (std_in_fd < 0 || std_out_fd < 0 || std_err_fd < 0)
            {
                my_log::LOG(ERROR) << "运行时打开标准文件失败" << "\n"; 
                return -1; // 打开文件失败
            }

            pid_t id = fork();
            if (id < 0)
            {
                my_log::LOG(ERROR) << "运行时创建子进程失败" << "\n"; 
                close(std_in_fd);
                close(std_out_fd);
                close(std_err_fd);
                return -2; // 创建子进程失败
            }
            else if (id == 0)
            {
                // 子进程
                dup2(std_in_fd, 0);
                dup2(std_out_fd, 1);
                dup2(std_err_fd, 2);

                execl(exe_cute.c_str()/*要执行谁*/, exe_cute.c_str()/*想要在命令行怎么执行*/,nullptr);
                exit(1);
            }
            else
            {
                close(std_in_fd);
                close(std_out_fd);
                close(std_err_fd);
                int status = 0;//退出码
                waitpid(id, &status, 0);
                // 程序运行异常,一定是因为收到了信号
                my_log::LOG(INFO) << "运行完毕, info: " << (status & 0x7F) << "\n"; 
                return status & 0x7F;
            }
        }
    };
}
