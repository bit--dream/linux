#include "compiler.hpp"
#include "runner.hpp"
int main()
{
    std::string code = "code";
    my_compiler::Compiler::Compile(code);
    my_runner::Runner::Run(code);
    return 0;
}
