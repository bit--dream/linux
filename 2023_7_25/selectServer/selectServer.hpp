#ifndef __SELECT_SVR_H__
#define __SELECT_SVR_H__

#include <iostream>
#include <sys/select.h>
#include "Log.hpp"
#include "Sock.hpp"

using namespace std;

class SelectServer
{
public:
    SelectServer(const uint16_t &port = 8080)
        : _port(port)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        logMessage(DEBUG, "create base socket success");
    }

    void Start()
    {
        fd_set rdfs;

        FD_ZERO(&rdfs);
        while(true)
        {
            FD_SET(_listensock, &rdfs); // 将_listensock添加到读文件描述符集中
            int n = select(_listensock + 1, &rdfs, nullptr, nullptr, nullptr);

            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out ...");
                break;
            case -1:
                logMessage(WARNING, "select error: %d : %s", errno, strerror(errno));
            default:
                // 成功
                break;
            }
        }
    }
    ~SelectServer()
    {
        if(_listensock >= 0) 
            close(_listensock);
    }

private:
    uint16_t _port;
    int _listensock;
};

#endif