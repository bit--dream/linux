#include <iostream>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <ctime>
#include <sys/time.h>
#include <sys/select.h>
bool SetNonBlock(int fd)
{
    int f1 = fcntl(fd, F_GETFL); // 在底层获取当前fd对应的文件读写标志位
    if (f1 < 0)
        return false;
    fcntl(fd, F_SETFL, f1 | O_NONBLOCK); // 设置非阻塞
    return true;
}

int main()
{
    while (true)
    {
        std::cout << "time: " << (unsigned long)time(nullptr) << std::endl;
        struct timeval currtime = {0, 0};
        int n = gettimeofday(&currtime, nullptr);
        assert(n == 0);
        (void)n;
        std::cout << "gettimeofday: " << currtime.tv_sec << " . " << currtime.tv_usec << std::endl;
        sleep(1);
    }

    // SetNonBlock(0); // 只要设置一次,后续就都是非阻塞了
    // char buffer[1024];
    // while (true)
    // {
    //     sleep(1);

    //     errno = 0; // 重置errno
    //     ssize_t s = read(0, buffer, sizeof buffer - 1); // 如果出错,不仅仅是错误返回值,errno变量也会被设置

    //     if(s > 0)
    //     {
    //         buffer[s - 1] = 0;
    //         std::cout << "echo#" << buffer << " errno[----]: " << errno << " errstring: " << strerror(errno) << std::endl;
    //     }
    //     else
    //     {
    //         if(errno == EWOULDBLOCK)
    //         {
    //             std::cout << "当前0号fd数据没有就绪,请再试试吧 " << std::endl;
    //             continue;
    //         }
    //         else if(errno ==  EINTR)
    //         {
    //             std::cout << "当前IO可能被信号中断,请再试试吧 " << std::endl;
    //             continue;
    //         }
    //         else
    //         {
    //             // 差错处理
    //             std::cout << "read error " << " errno: " << errno << " errstring: " << strerror(errno) << std::endl;
    //         }
    //     }
    // }

    return 0;
}
