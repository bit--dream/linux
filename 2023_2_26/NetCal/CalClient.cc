#include <iostream>
#include "Sock.hpp"
#include "Protocol.hpp"

using namespace ns_protocol;

static void Usage()
{
    std::cout << "\nUsage: " << "./client serverIp serverPort\n"
              << std::endl;
}

// ./client server_ip server_port
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage();
        exit(1);
    }
    std::string server_ip = argv[1];
    uint16_t server_port = atoi(argv[2]);

    Sock sock;
    int sockfd = sock.Socket();
    if(!sock.Connect(sockfd, server_ip, server_port))
    {
        std::cerr << "Connect error" << std::endl;
        exit(2);
    }
    
    while(true)
    {
        ns_protocol::Request req;
        std::cout << "请输入计算表达式:>";
        std::cin >> req._x >> req._op >> req._y;

        std::string s = req.Serialize();
        Send(sockfd, s);

        std::string r = Recv(sockfd);
        ns_protocol::Response resp;
        resp.Deserialize(r);
        std::cout << "code: " << resp._code << " | " << "result: " << resp._result << std::endl;
        sleep(1);
    }
    return 0;
}