#include <memory>

#include "TcpServer.hpp"
#include "Protocol.hpp"
#include <signal.h>

static void Usage()
{
    std::cout << "./server port \n"
              << std::endl;
}

static ns_protocol::Response calculatorHelp(const ns_protocol::Request &req)
{
    ns_protocol::Response resp(0, 0);
    switch (req._op)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '/':
        if (0 == req._y)
            resp._code = 1;
        else
            resp._result = req._x / req._y;
        break;
    case '%':
        if (0 == req._y)
            resp._code = 2;
        else
            resp._result = req._x % req._y;
        break;
    default:
        resp._code = 3;
        break;
    }
    return resp;
}

void calculator(int sock)
{
    std::string inbuffer;
    while (true)
    {
        // 1 读取数据成功
        bool res = ns_protocol::Recv(sock, &inbuffer);
        if (!res)
            break;
        // 2 协议解析,保证能读到一个完整的报文
        std::string package = ns_protocol::Decode(inbuffer);
        if (package.empty())
            continue;
        
        ns_protocol::Request req;
        req.Deserialize(package); // 反序列化

        ns_protocol::Response resp = calculatorHelp(req);

        // 结构化 -> 序列化
        std::string respStr = resp.Serialize(); // 对计算结果序列化
        ns_protocol::Send(sock, respStr);
    }
}

// void handler(int signo)
// {
//     std::cout << "get a signo : " << signo << std::endl;
//     exit(1);
// }

int main(int argc, char *argv[])
{
    // 防止非法写入
    signal(SIGPIPE, SIG_IGN);
    if (argc != 2)
    {
        Usage();
        exit(1);
    }

    std::unique_ptr<ns_tcpserver::TcpServer> server(new ns_tcpserver::TcpServer(atoi(argv[1])));
    server->BindService(calculator);
    server->Start();

    // ns_protocol::Request req(123, 12, '+');
    // std::string s = req.Serialize();
    // std::cout << s << std::endl;

    return 0;
}
