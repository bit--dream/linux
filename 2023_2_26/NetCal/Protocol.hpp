#pragma once
#include <iostream>
#include <cstring>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>

#define SIZE 1024

namespace ns_protocol
{

#define MYSELF 1
#define SPACE " "
#define SPACE_LEN strlen(SPACE)

#define SEP "\r\n"
#define SEP_LEN strlen(SEP) //
    class Request
    {
    public:
        // "length\r\n_x _op _y\r\nlength\r\n_x _op _y\r\nlength\r\n_x _op _y\r\n"
        //
        std::string Serialize() // 序列化
        {
#ifdef MYSELF
            std::string str;
            str = std::to_string(_x);
            str += SPACE;
            str += _op;
            str += SPACE;
            str += std::to_string(_y);

            return str;
#else
            std::cout << "TODD" << std::endl;
#endif
        }

        bool Deserialize(const std::string &package) //  反序列化
        {
#ifdef MYSELF
            std::size_t left = package.find(SPACE);
            if (left == std::string::npos)
                return false;
            std::size_t right = package.rfind(SPACE);
            if (left == std::string::npos)
                return false;

            _x = atoi(package.substr(0, left).c_str());
            if (left + SPACE_LEN > package.size())
                return false;
            else
                _op = package[left + SPACE_LEN];
            _y = atoi(package.substr(right + SPACE_LEN).c_str());

            return true;

#else
            std::cout << "TODD" << std::endl;
#endif
        }

    public:
        Request()
        {
        }

        Request(int x, int y, char op) : _x(x), _y(y), _op(op)
        {
        }

        ~Request() {}

    public:
        int _x;
        int _y;
        char _op;
    };

    class Response
    {
    public:
        std::string Serialize()
        {
#ifdef MYSELF
            std::string s;
            s = std::to_string(_code);
            s += SPACE;
            s += std::to_string(_result);

            return s;

#else
            std::cout << "TODD" << std::endl;
#endif
        }
        bool Deserialize(const std::string &package)
        {
#ifdef MYSELF
            std::size_t pos = package.find(SPACE);
            if (pos == std::string::npos)
                return false;

            _code = atoi(package.substr(0, pos).c_str());
            _result = atoi(package.substr(pos + SPACE_LEN).c_str());

            return true;

#else
            std::cout << "TODD" << std::endl;
#endif
        }

    public:
        Response()
        {
        }
        Response(int result, int code) : _result(result), _code(code)
        {
        }
        ~Response() {}

    public:
        int _x;
        int _y;
        char _op;
        int _result; // 计算结果
        int _code;   // 计算结果的状态码
    };

    // 一次要读一个完整的报文
    bool Recv(int sock, std::string *out)
    {
        //
        char buffer[1024];
        ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
        if (s > 0)
        {
            buffer[s] = 0;
            *out += buffer;
        }
        else if (s == 0) // 对方关了
        {
            std::cout << "client quit" << std::endl;
            return false;
        }
        else
        {
            std::cout << "recv error" << std::endl;
            return false;
        }
        return true;
    }

    void Send(int sock, const std::string str)
    {
        std::cout << "send in" << std::endl;
        int n = send(sock, str.c_str(), str.size(), 0);
        if (n < 0)
        {
            std::cout << "send error" << std::endl;
        }
    }

    // "length\r\n_x _op _y\r\n"
    std::string Decode(std::string &buffer)
    {
        std::size_t pos = buffer.find(SEP);
        if (pos == std::string::npos)
            return "";
        int size = atoi(buffer.substr(0, pos).c_str());  // length
        int surplus = buffer.size() - pos - SEP_LEN * 2; // 有效载荷
        if (surplus >= size)
        {
            // 至少有一个完整的报文
            buffer.erase(0, pos + SEP_LEN);
            std::string s = buffer.substr(0, size);
            buffer.erase(0, size + SEP_LEN);
            return s;
        }
        else
        {
            return "";
        }
    }
    void EnCode(std::string &buffer)
    {
        
    }
}