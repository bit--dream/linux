#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <mutex>
#include <cassert>
#include <jsoncpp/json/json.h>

// #include "oj_model.hpp"
#include "oj_model2.hpp"
#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "oj_view.hpp"
#include "../comm/httplib.h"

namespace my_control
{
    // 提供服务的主机
    class Machine
    {
    public:
        std::string _ip;  // 编译服务的ip
        int _port;        // 编译服务的port
        uint64_t _load;   // 编译服务的负载
        std::mutex *_mtx; // mutex禁止拷贝,使用指针
    public:
        Machine() : _ip(""), _port(0), _load(0), _mtx(nullptr)
        {
        }
        ~Machine()
        {
        }

    public:
        // 递增负载
        void IncLoad()
        {
            if (_mtx)
                _mtx->lock();
            _load++;
            if (_mtx)
                _mtx->unlock();
        }
        // 递减负载
        void DecLoad()
        {
            if (_mtx)
                _mtx->lock();
            _load--;
            if (_mtx)
                _mtx->unlock();
        }

        // 重置load
        void ResetLoad()
        {
            if(_mtx) _mtx->lock();
            _load = 0;
            if(_mtx) _mtx->unlock();
        }

        // 获取主机负载
        uint64_t Load()
        {
            uint64_t load = 0;
            if (_mtx)
                _mtx->lock();
            load = _load;
            if (_mtx)
                _mtx->unlock();

            return load;
        }
    };

    const std::string service_machine = "./conf/service_machine.conf";
    // 负载均衡
    class LoadBlance
    {
    private:
        // 可以给我们提供编译服务的所有主机
        // 每一台主机都有自己的下标,充当当前主机的id
        std::vector<Machine> _machines;
        // 所有在线的主机id
        std::vector<int> _online;
        // 所有离线的主机id
        std::vector<int> _offline;
        // 保证loadBlance它的数据安全
        std::mutex _mtx;

    public:
        LoadBlance()
        {
            assert(LoadConf(service_machine));
            my_log::LOG(INFO) << " 加载 " << service_machine << "成功" << std::endl;
        }
        ~LoadBlance()
        {
        }

    public:
        // 加载配置文件
        bool LoadConf(const std::string &machine_conf)
        {
            std::ifstream in(machine_conf);
            if (!in.is_open())
            {
                my_log::LOG(FATAL) << " 加载: " << machine_conf << " 失败 " << std::endl;
                return false;
            }
            std::string line;
           
            while (std::getline(in, line))
            { 
                std::vector<std::string> tokens;
                my_util::StringUtil::SplitString(line, &tokens, ":");
                if (tokens.size() != 2)
                {
                    my_log::LOG(WARNING) << " 切分 " << line << " 失败 " << std::endl;
                    continue;
                }
                Machine m;
                m._ip = tokens[0];
                m._port = atoi(tokens[1].c_str());
                m._load = 0;
                m._mtx = new std::mutex;

                _online.push_back(_machines.size());
                _machines.push_back(m);
            }
            in.close();
            return true;
        }

        bool SmartChoice(int *id, Machine **m)
        {
            // 1. 使用选择好的主机 (更新该主机的负载)
            // 2. 我们需要可能离线的主机
            _mtx.lock();
            // 负载均衡的算法
            // 1. 随机 + hash
            // 2. 轮询 + hash
            int online_num = _online.size();
            if (online_num == 0)
            {
                _mtx.unlock();
                my_log::LOG(FATAL) << " 后端的所有编译主机全部离线,请运维的同事尽快查看" << std::endl;
                return false;
            }
            // 找到负载最小的机器
            *id = _online[0];
            *m = &_machines[_online[0]];
            uint64_t min_load = _machines[_online[0]].Load();
            for (int i = 1; i < online_num; i++)
            {
                uint64_t curr_load = _machines[_online[i]].Load();
                if (min_load > curr_load)
                {
                    min_load = curr_load;
                    *id = _online[i];
                    *m = &_machines[_online[i]];
                }
            }
            _mtx.unlock();
            return true;
        }

        void OfflineMachine(int id)
        {
            _mtx.lock();
            for(auto iter = _online.begin(); iter != _online.end(); iter++)
            {
                if(*iter == id)
                {
                    //找到了
                    _machines[id].ResetLoad();
                    _online.erase(iter);
                    _offline.push_back(id);
                    break; // break的存在,不需要考虑迭代器失效的问题
                } 
            }
            _mtx.unlock();
        }
        
        // 所有主机都离线的时候,统一上线
        void OnlineMachine()
        {
            _mtx.lock();

            _online.insert(_online.begin(), _offline.begin(), _offline.end());
            _offline.erase(_offline.begin(), _offline.end());
            _mtx.unlock();

            my_log::LOG(INFO) << "所有的主机又上线了" << "\n";
        }

        void ShowMachines()
        {
            _mtx.lock();
            std::cout << " 当前在线主机列表: ";
            for(auto &id: _online)
            {
                std::cout << id << " " << std::endl;
            }
            std::cout << " 当前离线主机列表: ";
            for(auto &id: _offline)
            {
                std::cout << id << " " << std::endl;
            }
            _mtx.unlock();
        }
    };

    // 核心业务逻辑的控制器
    class Control
    {
    private:
        my_model::Model _model;  // 提供后台数据
        my_view::View _view;     // 提供html渲染功能
        LoadBlance _load_blance; // 核心负载均衡器
    public:
        Control()
        {
        }
        ~Control()
        {
        }

        void RecoveryMachine()
        {
            _load_blance.OnlineMachine();
        }

        // 根据题目数据构建网页
        // html 输出型参数
        bool AllQuestions(std::string *html)
        {
            bool ret = true;
            std::vector<my_model::Question> all;
            if (_model.GetAllQuestions(&all))
            {
                std::sort(all.begin(), all.end(), [](const my_model::Question &q1, const my_model::Question &q2){
                    return atoi(q1.number.c_str()) < atoi(q2.number.c_str());
                });
                // 获取题目信息成功,将所有的题目数据构建成网页
                _view.AllExpandHtml(all, html);
            }
            else
            {
                *html = "获取题目列表失败,形成题目列表失败";
                ret = false;
            }
            return ret;
        }

        bool Question(std::string number, std::string *html)
        {
            bool ret = true;
            my_model::Question q;
            if (_model.GetOneQuestion(number, &q))
            {
                // 获取指定题目信息成功,将题目数据构建成网页
                _view.OneExpandHtml(q, html);
            }
            else
            {
                *html = "指定题目: " + number + " 不存在!";
                ret = false;
            }
            return ret;
        }

         //登录界面
        bool Login(std::string *html)
        {
            bool ret = true;
            _view.LoginExpandHtml(html);
            return ret;
        }

        //竞赛界面
        bool Competition(std::string *html)
        {
            bool ret = true;
            _view.CompetitionExpandHtml(html);
            return ret;
        }

        //讨论界面
        bool Talk(std::string *html)
        {
            bool ret = true;
            _view.TalkExpandHtml(html);
            return ret;
        }

        //求职界面
        bool Job(std::string *html)
        {
            bool ret = true;
            _view.JobExpandHtml(html);
            return ret;
        }

        //判题
        // id : 100
        // code : #include
        // input : ""
        void Judge(const std::string &number, const std::string in_json, std::string *out_json)
        {
            // my_log::LOG(DEBUG) << in_json << "\n number: " << number << std::endl;
            // 0. 根据题目编号,直接拿到对应的题目细节
            struct my_model::Question q;
            _model.GetOneQuestion(number, &q);

            // 1. in_json 进行反序列化,得到题目的id, 得到用户提交的源代码,input
            Json::Reader reader;
            Json::Value in_value;
            reader.parse(in_json, in_value);
            std::string code = in_value["code"].asString();

            // 2. 拼接用户代码 + 测试用例代码,形成新的代码
            Json::Value compile_value;
            compile_value["input"] = in_value["input"].asString();
            compile_value["code"] = code + "\n" + q.tail;
            compile_value["cpu_limit"] = q.cpu_limit;
            compile_value["mem_limit"] = q.mem_limit;
            Json::FastWriter writer;
            std::string compile_string = writer.write(compile_value);

            // 3. 选择负载最低的主机(差错处理)

            while (true)
            {
                int id = 0;
                Machine *m = nullptr;
                if (!_load_blance.SmartChoice(&id, &m))
                {
                    break;
                }

                // 4. 然后发送http请求,得到结果
                httplib::Client cli(m->_ip, m->_port);
                m->IncLoad();
                my_log::LOG(INFO) << " 选择主机成功, 主机id: " << id << " 详情: " << m->_ip << " : " << m->_port << " 当前主机的负载是: " << m->Load() << std::endl;

                if (auto res = cli.Post("/compile_and_run", compile_string, "application/json;charset=uft-8"))
                {
                    if (res->status == 200)
                    {
                        *out_json = res->body;
                        m->DecLoad();
                        my_log::LOG(INFO) << " 请求编译和运行服务成功... " << std::endl;

                        break;
                    }
         
                    m->DecLoad();
                }
                else
                {
                    // 请求失败
                    my_log::LOG(ERROR) << " 当前请求的主机id: " << id << " 详情: " << m->_ip << " : " << m->_port << "可能已离线" << std::endl;
                    _load_blance.OfflineMachine(id);
                    _load_blance.ShowMachines();
                }
            }

            // 5. *out_json = 将结果赋值给out_json
        }
    };
}
